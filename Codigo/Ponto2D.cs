﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control
{
    public struct Ponto2D
    {
        #region Fields

        private float _x;
        private float _y;

        #endregion Fields

        #region Properties

        public float X
        {
            get { return this._x; }
            set { this._x = value; }
        }

        public float Y
        {
            get { return this._y; }
            set { this._y = value; }
        }

        #endregion Properties

        #region Operators

        public static Ponto2D operator +(Ponto2D ponto1, Ponto2D ponto2)
        {
            return new Ponto2D() { X = ponto1.X + ponto2.X, Y = ponto1.Y + ponto2.Y };
        }

        public static Ponto2D operator -(Ponto2D ponto1, Ponto2D ponto2)
        {
            return new Ponto2D() { X = ponto1.X - ponto2.X, Y = ponto1.Y - ponto2.Y };
        }

        public static Ponto2D operator *(Ponto2D ponto1, Ponto2D ponto2)
        {
            return new Ponto2D() { X = ponto1.X * ponto2.X, Y = ponto1.Y * ponto2.Y };
        }

        public static Ponto2D operator /(Ponto2D ponto1, Ponto2D ponto2)
        {
            return new Ponto2D() { X = ponto1.X / ponto2.X, Y = ponto1.Y / ponto2.Y };
        }

        public static double operator |(Ponto2D ponto1, Ponto2D ponto2)
        {
            return (ponto1.X * ponto2.Y) + (ponto1.Y * ponto2.Y);
        }

        public static double operator ^(Ponto2D ponto1, Ponto2D ponto2)
        {
            return (ponto1.X * ponto2.Y) - (ponto1.X * ponto2.Y);
        }

        #endregion Operators
    }
}