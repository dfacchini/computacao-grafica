﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control
{
    public class Vetor2D
    {
        public float m_a, m_b;

        public Vetor2D()
        {
            m_a = m_b = 0;
        }

        public Vetor2D(Ponto2D pt1, Ponto2D pt2)
        {
            m_a = pt2.X - pt1.X;
            m_b = pt2.Y - pt1.Y;
        }

        public Vetor2D(float a, float b)
        {
            m_a = a; m_b = b;
        }

        public Vetor2D(Ponto2D pt)
        {
            m_a = pt.X;
            m_b = pt.Y;
        }

        public static implicit operator Vetor2D(float valor)
        {
            return new Vetor2D(valor, valor);
        }

        public static float operator *(Vetor2D u, Vetor2D v)
        {
            return u.m_a * v.m_a + u.m_b * v.m_b;
        }

        public float Modulo()
        {
            return (float)Math.Sqrt((m_a * m_a) + (m_b * m_b));
        }

        public static Vetor2D operator -(Vetor2D u)
        {
            return new Vetor2D(-u.m_a, -u.m_b);
        }

        public static double Determinante(Vetor2D u, Vetor2D v)
        {
            return u.m_a * v.m_b - u.m_b * v.m_a;
        }
    }
}



