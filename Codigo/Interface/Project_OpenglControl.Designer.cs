﻿using openGl_Control.Interface;
namespace openGl_Control
{
    partial class Project_OpenglControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Project_OpenglControl));
            this.tblPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.simpleOpenGl_control = new openGl_Control.Interface.ViewCtrl();
            this.tspTools = new System.Windows.Forms.ToolStrip();
            this.tsbConstruction = new System.Windows.Forms.ToolStripButton();
            this.tsbActivateMagnetIntersection = new System.Windows.Forms.ToolStripButton();
            this.tsbAddLine = new System.Windows.Forms.ToolStripButton();
            this.tsbSelectObject = new System.Windows.Forms.ToolStripButton();
            this.tsbDeleteLine = new System.Windows.Forms.ToolStripButton();
            this.tsbCleanView = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbView3D = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbInformations = new System.Windows.Forms.ToolStripButton();
            this.tsbHelp = new System.Windows.Forms.ToolStripButton();
            this.tblPrincipal.SuspendLayout();
            this.tspTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblPrincipal
            // 
            this.tblPrincipal.ColumnCount = 1;
            this.tblPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblPrincipal.Controls.Add(this.simpleOpenGl_control, 0, 1);
            this.tblPrincipal.Controls.Add(this.tspTools, 0, 0);
            this.tblPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tblPrincipal.Name = "tblPrincipal";
            this.tblPrincipal.RowCount = 2;
            this.tblPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblPrincipal.Size = new System.Drawing.Size(863, 720);
            this.tblPrincipal.TabIndex = 3;
            // 
            // simpleOpenGl_control
            // 
            this.simpleOpenGl_control.AccumBits = ((byte)(0));
            this.simpleOpenGl_control.AutoCheckErrors = false;
            this.simpleOpenGl_control.AutoFinish = false;
            this.simpleOpenGl_control.AutoMakeCurrent = true;
            this.simpleOpenGl_control.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.simpleOpenGl_control.AutoSwapBuffers = true;
            this.simpleOpenGl_control.BackColor = System.Drawing.Color.Black;
            this.simpleOpenGl_control.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.simpleOpenGl_control.ColorBits = ((byte)(32));
            this.simpleOpenGl_control.DepthBits = ((byte)(16));
            this.simpleOpenGl_control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleOpenGl_control.Location = new System.Drawing.Point(5, 47);
            this.simpleOpenGl_control.Margin = new System.Windows.Forms.Padding(5);
            this.simpleOpenGl_control.Name = "simpleOpenGl_control";
            this.simpleOpenGl_control.Size = new System.Drawing.Size(853, 668);
            this.simpleOpenGl_control.StencilBits = ((byte)(0));
            this.simpleOpenGl_control.TabIndex = 2;
            this.simpleOpenGl_control.KeyDown += new System.Windows.Forms.KeyEventHandler(this.simpleOpenGl_control_KeyDown);
            this.simpleOpenGl_control.KeyUp += new System.Windows.Forms.KeyEventHandler(this.simpleOpenGl_control_KeyUp);
            this.simpleOpenGl_control.MouseDown += new System.Windows.Forms.MouseEventHandler(this.simpleOpenGl_control_MouseDown);
            this.simpleOpenGl_control.MouseMove += new System.Windows.Forms.MouseEventHandler(this.simpleOpenGl_control_MouseMove);
            this.simpleOpenGl_control.MouseUp += new System.Windows.Forms.MouseEventHandler(this.simpleOpenGl_control_MouseUp);
            // 
            // tspTools
            // 
            this.tspTools.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tspTools.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tspTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbConstruction,
            this.tsbActivateMagnetIntersection,
            this.tsbAddLine,
            this.tsbSelectObject,
            this.tsbDeleteLine,
            this.tsbCleanView,
            this.toolStripSeparator1,
            this.tsbView3D,
            this.toolStripSeparator2,
            this.tsbInformations,
            this.tsbHelp});
            this.tspTools.Location = new System.Drawing.Point(1, 1);
            this.tspTools.Margin = new System.Windows.Forms.Padding(1);
            this.tspTools.Name = "tspTools";
            this.tspTools.Size = new System.Drawing.Size(861, 40);
            this.tspTools.TabIndex = 3;
            this.tspTools.Text = "Limpar ViewPort";
            // 
            // tsbConstruction
            // 
            this.tsbConstruction.AutoSize = false;
            this.tsbConstruction.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbConstruction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tsbConstruction.Checked = true;
            this.tsbConstruction.CheckOnClick = true;
            this.tsbConstruction.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbConstruction.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbConstruction.Image = ((System.Drawing.Image)(resources.GetObject("tsbConstruction.Image")));
            this.tsbConstruction.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbConstruction.Margin = new System.Windows.Forms.Padding(4);
            this.tsbConstruction.Name = "tsbConstruction";
            this.tsbConstruction.Size = new System.Drawing.Size(32, 32);
            this.tsbConstruction.Text = "Construção";
            this.tsbConstruction.Click += new System.EventHandler(this.tsbConstruction_Click);
            // 
            // tsbActivateMagnetIntersection
            // 
            this.tsbActivateMagnetIntersection.AutoSize = false;
            this.tsbActivateMagnetIntersection.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbActivateMagnetIntersection.CheckOnClick = true;
            this.tsbActivateMagnetIntersection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbActivateMagnetIntersection.Image = ((System.Drawing.Image)(resources.GetObject("tsbActivateMagnetIntersection.Image")));
            this.tsbActivateMagnetIntersection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbActivateMagnetIntersection.Margin = new System.Windows.Forms.Padding(4);
            this.tsbActivateMagnetIntersection.Name = "tsbActivateMagnetIntersection";
            this.tsbActivateMagnetIntersection.Size = new System.Drawing.Size(32, 32);
            this.tsbActivateMagnetIntersection.Text = "toolStripButton1";
            this.tsbActivateMagnetIntersection.Click += new System.EventHandler(this.tsbActivateMagnetIntersection_Click);
            // 
            // tsbAddLine
            // 
            this.tsbAddLine.AutoSize = false;
            this.tsbAddLine.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbAddLine.CheckOnClick = true;
            this.tsbAddLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddLine.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddLine.Image")));
            this.tsbAddLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddLine.Margin = new System.Windows.Forms.Padding(4);
            this.tsbAddLine.Name = "tsbAddLine";
            this.tsbAddLine.Size = new System.Drawing.Size(32, 32);
            this.tsbAddLine.Text = "Adicionar linha";
            this.tsbAddLine.Click += new System.EventHandler(this.tsbAddLine_Click);
            // 
            // tsbSelectObject
            // 
            this.tsbSelectObject.AutoSize = false;
            this.tsbSelectObject.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbSelectObject.CheckOnClick = true;
            this.tsbSelectObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSelectObject.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelectObject.Image")));
            this.tsbSelectObject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSelectObject.Margin = new System.Windows.Forms.Padding(4);
            this.tsbSelectObject.Name = "tsbSelectObject";
            this.tsbSelectObject.Size = new System.Drawing.Size(32, 32);
            this.tsbSelectObject.Text = "Selecionar Objeto";
            this.tsbSelectObject.Click += new System.EventHandler(this.tsbSelectLine_Click);
            // 
            // tsbDeleteLine
            // 
            this.tsbDeleteLine.AutoSize = false;
            this.tsbDeleteLine.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbDeleteLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDeleteLine.Enabled = false;
            this.tsbDeleteLine.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeleteLine.Image")));
            this.tsbDeleteLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeleteLine.Margin = new System.Windows.Forms.Padding(4);
            this.tsbDeleteLine.Name = "tsbDeleteLine";
            this.tsbDeleteLine.Size = new System.Drawing.Size(32, 32);
            this.tsbDeleteLine.Text = "Remover linha";
            this.tsbDeleteLine.Click += new System.EventHandler(this.tsbDeleteLine_Click);
            // 
            // tsbCleanView
            // 
            this.tsbCleanView.AutoSize = false;
            this.tsbCleanView.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbCleanView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCleanView.Image = ((System.Drawing.Image)(resources.GetObject("tsbCleanView.Image")));
            this.tsbCleanView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCleanView.Margin = new System.Windows.Forms.Padding(4);
            this.tsbCleanView.Name = "tsbCleanView";
            this.tsbCleanView.Size = new System.Drawing.Size(32, 32);
            this.tsbCleanView.Text = "Limpar View";
            this.tsbCleanView.Click += new System.EventHandler(this.tsbCleanView_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 40);
            // 
            // tsbView3D
            // 
            this.tsbView3D.AutoSize = false;
            this.tsbView3D.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbView3D.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbView3D.Image = ((System.Drawing.Image)(resources.GetObject("tsbView3D.Image")));
            this.tsbView3D.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbView3D.Margin = new System.Windows.Forms.Padding(4);
            this.tsbView3D.Name = "tsbView3D";
            this.tsbView3D.Size = new System.Drawing.Size(32, 32);
            this.tsbView3D.Text = "Visualização em 3D";
            this.tsbView3D.Click += new System.EventHandler(this.tsbView3D_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 40);
            // 
            // tsbInformations
            // 
            this.tsbInformations.AutoSize = false;
            this.tsbInformations.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbInformations.CheckOnClick = true;
            this.tsbInformations.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbInformations.Enabled = false;
            this.tsbInformations.Image = ((System.Drawing.Image)(resources.GetObject("tsbInformations.Image")));
            this.tsbInformations.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInformations.Margin = new System.Windows.Forms.Padding(4);
            this.tsbInformations.Name = "tsbInformations";
            this.tsbInformations.Size = new System.Drawing.Size(32, 32);
            this.tsbInformations.Text = "Informações";
            // 
            // tsbHelp
            // 
            this.tsbHelp.AutoSize = false;
            this.tsbHelp.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tsbHelp.CheckOnClick = true;
            this.tsbHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbHelp.Enabled = false;
            this.tsbHelp.Image = ((System.Drawing.Image)(resources.GetObject("tsbHelp.Image")));
            this.tsbHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbHelp.Margin = new System.Windows.Forms.Padding(4);
            this.tsbHelp.Name = "tsbHelp";
            this.tsbHelp.Size = new System.Drawing.Size(32, 32);
            this.tsbHelp.Text = "Ajuda";
            // 
            // Project_OpenglControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(863, 720);
            this.Controls.Add(this.tblPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Project_OpenglControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View";
            this.Shown += new System.EventHandler(this.MainOpenglControl_Shown);
            this.tblPrincipal.ResumeLayout(false);
            this.tblPrincipal.PerformLayout();
            this.tspTools.ResumeLayout(false);
            this.tspTools.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ViewCtrl simpleOpenGl_control;
        private System.Windows.Forms.TableLayoutPanel tblPrincipal;
        private System.Windows.Forms.ToolStrip tspTools;
        private System.Windows.Forms.ToolStripButton tsbConstruction;
        private System.Windows.Forms.ToolStripButton tsbAddLine;
        private System.Windows.Forms.ToolStripButton tsbDeleteLine;
        private System.Windows.Forms.ToolStripButton tsbSelectObject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbHelp;
        private System.Windows.Forms.ToolStripButton tsbInformations;
        private System.Windows.Forms.ToolStripButton tsbCleanView;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbView3D;
        private System.Windows.Forms.ToolStripButton tsbActivateMagnetIntersection;
    }
}

