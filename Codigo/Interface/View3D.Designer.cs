﻿namespace openGl_Control.Interface
{
    partial class View3D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View3D));
            this.tblView = new System.Windows.Forms.TableLayoutPanel();
            this.view = new openGl_Control.Interface.ViewCtrl();
            this.tblView.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblView
            // 
            this.tblView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tblView.ColumnCount = 1;
            this.tblView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblView.Controls.Add(this.view, 0, 0);
            this.tblView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblView.Location = new System.Drawing.Point(0, 0);
            this.tblView.Margin = new System.Windows.Forms.Padding(8);
            this.tblView.Name = "tblView";
            this.tblView.RowCount = 1;
            this.tblView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblView.Size = new System.Drawing.Size(742, 449);
            this.tblView.TabIndex = 0;
            // 
            // view
            // 
            this.view.AccumBits = ((byte)(0));
            this.view.AutoCheckErrors = false;
            this.view.AutoFinish = false;
            this.view.AutoMakeCurrent = true;
            this.view.AutoSwapBuffers = true;
            this.view.BackColor = System.Drawing.Color.Black;
            this.view.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.view.ColorBits = ((byte)(32));
            this.view.DepthBits = ((byte)(16));
            this.view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.view.Location = new System.Drawing.Point(3, 3);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(736, 443);
            this.view.StencilBits = ((byte)(0));
            this.view.TabIndex = 0;
            this.view.MouseDown += new System.Windows.Forms.MouseEventHandler(this.View_MouseDown);
            this.view.MouseMove += new System.Windows.Forms.MouseEventHandler(this.View_MouseMove);
            this.view.MouseUp += new System.Windows.Forms.MouseEventHandler(this.View_MouseUp);
            this.view.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.View_MouseWheel);
            // 
            // View3D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(742, 449);
            this.Controls.Add(this.tblView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "View3D";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "View3D";
            this.Shown += new System.EventHandler(this.View3D_Shown);
            this.tblView.ResumeLayout(false);
            this.ResumeLayout(false);

        }        

        #endregion

        private System.Windows.Forms.TableLayoutPanel tblView;
        private ViewCtrl view;

    }
}