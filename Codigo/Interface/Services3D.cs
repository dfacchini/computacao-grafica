﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace openGl_Control.Interface
{
    public class Services3D
    {
        #region Fields

        private World3D world;
        private string _file;
        private World3D _world3D;

        #endregion

        #region Properties

        private World3D ServiceWorld3D
        {
            get { return this._world3D; }
            set { this._world3D = value; }
        }


        #endregion

        #region Constructor

        public Services3D(int height, int width, string file)
            : this(file)
        {
            this.SetView(height, width);
        }

        public Services3D(string file)
        {
            this._world3D = new World3D();
            this._file = file;
        }

        #endregion

        #region Private Methods

        #endregion

        #region Internal Methods

        internal void SetView(int height, int width)
        {
            if (this.ServiceWorld3D == null) return;
            this.ServiceWorld3D.SetView(height, width);
        }

        internal void RenderWorld()
        {
            if (this.ServiceWorld3D == null) return;
            this.ServiceWorld3D.RenderScene();
        }

        #endregion

        #region Public Methods

        #endregion
    }
}
