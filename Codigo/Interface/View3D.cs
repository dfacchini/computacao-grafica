﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using openGl_Control.Elements;

namespace openGl_Control.Interface
{
    public partial class View3D : Form
    {
        #region Properties

        private World3D world = null;
        private bool m_isCharging = false;
        private float _panX, _panY;
        private float _inPanX, _inPanY;
        private float PanX, PanY;

        private float _rotationX, _rotationy;
        private float _inRotationX, _inRotationy;
        private float RotationX, Rotationy;

        public List<Ponto3D> pontos = null;
        private List<Ponto3D> pontosB = null;
        private List<Ponto3D> height = null;

        #endregion Properties

        #region Constructor

        public View3D(string file)
        {
            this.m_isCharging = true;
            this.world = new World3D();
            this.pontos = new List<Ponto3D>();
            this.pontosB = new List<Ponto3D>();
            this.height = new List<Ponto3D>();

            InitializeComponent();
            this.view.InitializeContexts();

            this.world.InitGl();
            this.world.InitPrimitives();
            this.world.SetView(this.Height, this.Width);
            this.ReadFile(file);
            
            this.DrawItems();
            this.world.InitPrimitives();
            this.world.RenderScene();
            this.m_isCharging = false;
        }

        #endregion Constructor

        #region Internal Methods

        internal void ReadFile(string file)
        {
            using (StreamReader reader = new StreamReader(file))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Equals("=Ambient=", StringComparison.OrdinalIgnoreCase))
                        continue;
                    if (!string.IsNullOrEmpty(line))
                    {
                        string[] elements = line.Split(' ');
                        Ponto3D pnt = new Ponto3D() { X = float.Parse(elements[1]), Y = 2.0f, Z = float.Parse(elements[2]) };
                        pontos.Add(pnt);
                    }
                }
            }

            foreach (Ponto3D ponto in pontos)
            {
                pontosB.Add(new Ponto3D() { X = ponto.X, Y = -2.0f, Z = ponto.Z });
            }

            for (int i = 0; i < pontos.Count; i++)
            {
                height.Add(pontos[i]);
                height.Add(pontosB[i]);
            }
        }

        internal void DrawItems()
        {
            world.drawableObjects.Add(new SimpleMovimentation(this.pontos, this.pontosB, this.height));
        }

        #endregion

        #region Override Methods

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (!this.m_isCharging)
            {
                this.world.SetView(this.Height, this.Width);
                this.Refresh();
                this.world.RenderScene();
            }
        }

        #endregion Override Methods

        #region Events

        private void View_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Middle)
            //{
            //    float deltaX = _panX - e.X;
            //    float deltaY = _panY - e.Y;

            //    PanY = _inPanY + deltaY / 25;
            //    PanX = _inPanX - deltaX / 25;
            //    this.world.UpdatePans(new Ponto2D() { X = e.X, Y = e.Y });
            //    this.Refresh();
            //    this.world.RenderScene();
            //}
            //else 
            if (e.Button != System.Windows.Forms.MouseButtons.None)
            {
                float deltay = _rotationy - e.Y;
                float deltaX = _rotationX - e.X;

                Rotationy = _inRotationy - deltaX;
                RotationX = _inRotationX - deltay;
                this.world.UpdateLocations(RotationX, Rotationy);
                this.Refresh();
                this.world.RenderScene();

            }
        }

        private void View_MouseDown(object sender, MouseEventArgs e)
        {
            Console.WriteLine("X = " + e.X + "Y = " + e.Y);
            _rotationX = e.X;
            _rotationy = e.Y;
            _panX = e.X;
            _panY = e.Y;
            this._inPanX = this.PanX;
            this._inPanY = this.PanY;

            this._inRotationX = this.RotationX;
            this._inRotationy = this.Rotationy;
            this.Refresh();
            this.world.RenderScene();
        }

        private void View_MouseUp(object sender, MouseEventArgs e)
        {
            this.Refresh();
            this.world.RenderScene();
        }

        private void View_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.world.UpdateZoom(e.Delta);
            this.Refresh();
            this.world.RenderScene();
        }

        private void MainOpenglControl_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            this.world.RenderScene();
        }

        private void View3D_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            if (this.world != null)
                this.world.RenderScene();
        }

        #endregion Events
    }
}
