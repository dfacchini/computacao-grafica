﻿using System;
using System.Windows.Forms;
using openGl_Control.Elements;
using openGl_Control.Elements.Enums;
using openGl_Control.Interface;

namespace openGl_Control
{
    public partial class Project_OpenglControl : Form
    {
        #region Fields

        private bool m_isCharging = false;

        private Services _service = null;
        private EMode _activeMode = EMode.Construction;
        private bool _canDelete = false;

        #endregion Fields

        #region Properties

        private Services Service
        {
            get { return this._service; }
            set { this._service = value; }
        }

        #endregion

        #region Constructor

        public Project_OpenglControl(World world)
        {

            this.m_isCharging = true;
            if (world == null)
                throw new ArgumentNullException("world");

            InitializeComponent();

            this.simpleOpenGl_control.InitializeContexts();

            this.Service = new Services(world, this.simpleOpenGl_control.Height, this.simpleOpenGl_control.Width) { ClientHeight = this.simpleOpenGl_control.ClientSize.Height };

            this.Service.RefreshViewNeeded += Service_RefreshViewNeeded;
            this.m_isCharging = false;
        }

        #endregion Constructor

        #region Private Methods

        private void ClearWorldFunctions()
        {
            this.tsbDeleteLine.Enabled = false;
            this.Service.ClearInternDraws();
            this.Refresh();
            this.Service.RenderWorld();
        }

        private void ClearToolSelections()
        {
            switch (this._activeMode)
            {
                case EMode.Construction:
                    this.tsbAddLine.Checked = false;
                    this.tsbSelectObject.Checked = false;
                    this.ClearWorldFunctions();
                    break;
                case EMode.AddLine:
                    this.tsbConstruction.Checked = false;
                    this.tsbSelectObject.Checked = false;
                    this.ClearWorldFunctions();
                    break;
                case EMode.SelectObject:
                    this.tsbConstruction.Checked = false;
                    this.tsbAddLine.Checked = false;
                    this.ClearWorldFunctions();
                    break;
                default:
                    this.tsbConstruction.Checked = false;
                    this.tsbAddLine.Checked = false;
                    this.tsbAddLine.Checked = false;
                    this.tsbSelectObject.Checked = false;
                    this.tsbInformations.Checked = false;
                    this.tsbHelp.Checked = false;
                    this.tsbView3D.Checked = false;
                    this.ClearWorldFunctions();
                    break;
            }
        }

        private void SetMode(EMode mode)
        {
            this._activeMode = mode;

            if (this._activeMode == EMode.None)
            {
                this.Service.ClearInternDraws();
                this.Refresh();
            }
            else
                ClearToolSelections();
        }

        #endregion

        #region Override Methods

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (!this.m_isCharging)
            {
                this.Service.SetView(this.simpleOpenGl_control.Height, this.simpleOpenGl_control.Width);
                this.Service.ClientHeight = this.simpleOpenGl_control.ClientSize.Height;
                this.Refresh();
                this.Service.RenderWorld();
            }
        }

        #endregion Override Methods

        #region Events

        private void MainOpenglControl_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            this.Service.RenderWorld();
        }

        private void Service_RefreshViewNeeded()
        {
            this.Refresh();
        }

        #endregion Events

        #region OpenGl Control Events

        private void simpleOpenGl_control_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
        }

        private void simpleOpenGl_control_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (this._canDelete)
                {
                    this.Service.DeleteObject();
                    this.ClearToolSelections();
                    this.tsbDeleteLine.Enabled = false;
                }
            }
        }

        private void simpleOpenGl_control_MouseMove(object sender, MouseEventArgs e)
        {
            if (this._activeMode == EMode.None) return;

            if (e.Button == System.Windows.Forms.MouseButtons.Middle && this._activeMode != EMode.Construction)
            {
                this.Service.MoveLine(e);
                return;
            }

            switch (this._activeMode)
            {
                case EMode.Construction:
                    this.Service.MoveLine(e);
                    break;
            }
        }

        private void simpleOpenGl_control_MouseDown(object sender, MouseEventArgs e)
        {
            if (this._activeMode == EMode.None) return;

            if (e.Button == System.Windows.Forms.MouseButtons.Middle)
            {
                this.tsbConstruction.Checked = true;
                this.Service.BuildShape(e);
                return;
            }

            switch (this._activeMode)
            {
                case EMode.Construction:
                    this.Service.BuildShape(e);
                    break;
                case EMode.AddLine:
                    this.Service.BuildLine(e);
                    break;
                case EMode.SelectObject:
                    this.Service.SelectObject(e, ref this._canDelete);
                    this.tsbDeleteLine.Enabled = _canDelete;
                    break;
            }
        }

        private void simpleOpenGl_control_MouseUp(object sender, MouseEventArgs e)
        {
            if (this._activeMode == EMode.None) return;

            if (this._activeMode != EMode.Construction && e.Button == System.Windows.Forms.MouseButtons.Middle)
                this.tsbConstruction.Checked = false;

            this.Refresh();
            this.Service.RenderWorld(false, e.X, e.Y);
        }

        #endregion OpenGl Control Events

        #region Tools Events

        private void tsbConstruction_Click(object sender, EventArgs e)
        {
            this.SetMode(this.tsbConstruction.Checked ? EMode.Construction : EMode.None);
        }

        private void tsbActivateMagnetIntersection_Click(object sender, EventArgs e)
        {
            if (this.Service != null)
                this.Service.SetMagnet(this.tsbActivateMagnetIntersection.Checked);
        }

        private void tsbAddLine_Click(object sender, EventArgs e)
        {
            this.SetMode(this.tsbAddLine.Checked ? EMode.AddLine : EMode.None);
        }

        private void tsbSelectLine_Click(object sender, EventArgs e)
        {
            this.SetMode(this.tsbSelectObject.Checked ? EMode.SelectObject : EMode.None);
        }

        private void tsbDeleteLine_Click(object sender, EventArgs e)
        {
            this.Service.DeleteObject();
            this.ClearToolSelections();
            this.tsbDeleteLine.Enabled = false;
        }

        private void tsbCleanView_Click(object sender, EventArgs e)
        {
            if (this.Service != null)
            {
                this._activeMode = EMode.None;
                this.ClearToolSelections();
                this.Service.ClearView();
                this.Refresh();

                this._activeMode = EMode.Construction;
                this.tsbConstruction.Checked = true;
            }
        }

        private void tsbView3D_Click(object sender, EventArgs e)
        {
            if (this.Service != null)
                this.Service.BuildFile3D();
        }

        #endregion
    }
}