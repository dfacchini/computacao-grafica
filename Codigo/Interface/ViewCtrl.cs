﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace openGl_Control.Interface
{
    public class ViewCtrl : Tao.Platform.Windows.SimpleOpenGlControl
    {
        #region Override Methods

        protected override bool IsInputKey(System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Left)
                return true;

            if (keyData == Keys.Right)
                return true;

            if (keyData == Keys.Down)
                return true;

            if (keyData == Keys.Up)
                return true;

            return base.IsInputKey(keyData);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
        }

        #endregion
    }
}