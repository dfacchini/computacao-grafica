﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace openGl_Control.Interface
{
    public class Services
    {
        #region Fields

        private World _world = null;

        private float _panX, _panY;
        private float _inPanX, _inPanY;
        private float PanX, PanY;
        private int _currentID;
        private int _clientHeight;

        #endregion

        #region Properties

        public int ClientHeight
        {
            get { return this._clientHeight; }
            set { this._clientHeight = value; }
        }

        private World ServiceWorld
        {
            get { return this._world; }
            set { this._world = value; }
        }

        #endregion

        #region Constructor

        public Services(World world, int height, int width)
        {
            this.ServiceWorld = world;
            this.ServiceWorld.InitGl();
            this.ServiceWorld.InitPrimitives();
            this.ServiceWorld.SetView(height, width);
        }

        #endregion

        #region Events

        public delegate void RefreshView();
        public event RefreshView RefreshViewNeeded;

        #endregion

        #region Virtual Methods

        internal virtual void RefreshTrigger()
        {
            if (this.RefreshViewNeeded != null)
                this.RefreshViewNeeded();
        }

        #endregion

        #region Internal Methods

        internal void SetView(int height, int width)
        {
            if (this.ServiceWorld == null) return;
            this.ServiceWorld.SetView(height, width);
        }

        internal void RenderWorld()
        {
            if (this.ServiceWorld == null) return;
            this.ServiceWorld.RenderScene();
        }

        internal void RenderWorld(bool picking, float x, float y)
        {
            if (this.ServiceWorld == null) return;
            this.ServiceWorld.RenderScene(picking, new Ponto2D() { X = x, Y = y });
        }

        internal void DeleteObject()
        {
            if (this.ServiceWorld == null) return;
            this.ServiceWorld.DeleteSelectedObject();
        }

        internal void ClearInternDraws()
        {
            if (this.ServiceWorld == null) return;

            this.ServiceWorld.InterCreated = false;
            this.ServiceWorld.ClearInterLine();
            this.ServiceWorld.ClearSelectedLine();
            this.RenderWorld();
        }

        internal void BuildLine(MouseEventArgs e)
        {
            if (this.ServiceWorld == null) return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.ServiceWorld.CreateLine(new Ponto2D() { X = e.X, Y = e.Y }, this.ClientHeight);
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene(false, new Ponto2D() { X = e.X, Y = e.Y });
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                this.ServiceWorld.ClearInterLine();
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene(false, new Ponto2D() { X = e.X, Y = e.Y });
            }
        }

        internal void SelectObject(MouseEventArgs e, ref bool selectedLine)
        {
            if (this.ServiceWorld == null) return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                selectedLine = this.ServiceWorld.SelectDrawableObject(new Ponto2D() { X = e.X, Y = e.Y }, this.ClientHeight);

                this.RefreshTrigger();
                this.ServiceWorld.RenderScene();
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                this.ServiceWorld.ClearSelectedLine();
                this.ServiceWorld.ClearInterLine();
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene();
            }
        }

        internal void MoveLine(MouseEventArgs e)
        {
            if (this.ServiceWorld == null) return;

            if (ServiceWorld.InterCreated)
            {
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene(false, new Ponto2D() { X = e.X, Y = e.Y });
            }

            if (e.Button == MouseButtons.Middle)
            {
                float deltaX = _panX - e.X;
                float deltaY = _panY - e.Y;

                PanY = _inPanY + deltaY / 25;
                PanX = _inPanX - deltaX / 25;
                this.ServiceWorld.UpdatePans(this._currentID, new Ponto2D() { X = e.X, Y = e.Y }, this.ClientHeight);
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene();
            }
        }

        internal void BuildShape(MouseEventArgs e)
        {
            if (this.ServiceWorld == null) return;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ServiceWorld.InterCreated = false;
                this.ServiceWorld.Pick(new Ponto2D() { X = e.X, Y = this.ClientHeight - e.Y - 1 });
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene(false, new Ponto2D() { X = e.X, Y = e.Y });
                return;
            }
            else
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Middle)
                {
                    ServiceWorld.InterCreated = false;
                    this._currentID = this.ServiceWorld.Pick(new Ponto2D() { X = e.X, Y = this.ClientHeight - e.Y - 1 }, 5, 5);
                    if (_currentID < 0)
                    {
                        this.RefreshTrigger();
                        this.ServiceWorld.RenderScene(false, new Ponto2D() { X = e.X, Y = e.Y });
                        return;
                    }
                    _panX = e.X;
                    _panY = e.Y;
                    this._inPanX = this.PanX;
                    this._inPanY = this.PanY;
                }
                else
                {
                    this.ServiceWorld.CreateIntersection(new Ponto2D() { X = e.X, Y = e.Y }, this.ClientHeight);

                    _panX = e.X;
                    _panY = e.Y;
                    this._inPanX = this.PanX;
                    this._inPanY = this.PanY;
                }

                this.RefreshTrigger();
                this.ServiceWorld.RenderScene(false, new Ponto2D() { X = e.X, Y = e.Y });
            }
        }

        internal void ClearView()
        {
            if (this.ServiceWorld != null)
            {
                this.ServiceWorld.ClearViewPort();
                this.RefreshTrigger();
                this.ServiceWorld.RenderScene();
            }
        }

        internal void BuildFile3D()
        {
            if (this.ServiceWorld != null)
            {
                string file = this.ServiceWorld.Build3D();
                using (View3D view = new View3D(file))
                {
                    if (view.pontos.Count > 0)
                        view.ShowDialog();
                }
                this.RefreshTrigger();
                this.RenderWorld();
            }
        }

        internal void SetMagnet(bool magnetIntersection)
        {
            if (this.ServiceWorld != null)
                this.ServiceWorld.MagnetIntersection = magnetIntersection;
        }

        #endregion
    }
}
