﻿using System;

namespace openGl_Control
{
    public struct Ponto3D
    {
        #region Fields

        private float _x;
        private float _y;
        private float _z;

        #endregion

        #region Properties

        public float X
        {
            get
            {
                return this._x;
            }
            set
            {
                this._x = value;
            }
        }

        public float Y
        {
            get
            {
                return this._y;
            }
            set
            {
                this._y = value;
            }
        }

        public float Z
        {
            get
            {
                return this._z;
            }
            set
            {
                this._z = value;
            }
        }

        #endregion Properties

        #region Operators

        public static Ponto3D operator +(Ponto3D ponto1, Ponto3D ponto2)
        {
            return new Ponto3D() { X = ponto1.X + ponto2.X, Y = ponto1.Y + ponto2.Y, Z = ponto1.Z + ponto2.Z };
        }

        public static Ponto3D operator -(Ponto3D ponto1, Ponto3D ponto2)
        {
            return new Ponto3D() { X = ponto1.X - ponto2.X, Y = ponto1.Y - ponto2.Y, Z = ponto1.Z - ponto2.Z };
        }

        public static Ponto3D operator *(Ponto3D ponto1, Ponto3D ponto2)
        {
            return new Ponto3D() { X = ponto1.X * ponto2.X, Y = ponto1.Y * ponto2.Y, Z = ponto1.Z * ponto2.Z };
        }

        public static Ponto3D operator /(Ponto3D ponto1, Ponto3D ponto2)
        {
            return new Ponto3D() { X = ponto1.X / ponto2.X, Y = ponto1.Y / ponto2.Y, Z = ponto1.Z / ponto2.Z };
        }

        public static double operator |(Ponto3D ponto1, Ponto3D ponto2)
        {
            return (ponto1.X * ponto2.Y) + (ponto1.Y * ponto2.Y) + (ponto1.Z * ponto2.Z);
        }

        public static double operator ^(Ponto3D ponto1, Ponto3D ponto2)
        {
            return (ponto1.X * ponto2.Y) - (ponto1.X * ponto2.Y) - (ponto1.Z * ponto2.Z);
        }

        #endregion
    }
}