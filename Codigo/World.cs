﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using openGl_Control.Elements;
using openGl_Control.Elements.Enums;
using Tao.OpenGl;

namespace openGl_Control
{
    public class World
    {
        #region Fields

        private Axes axes;

        private Dictionary<int, IDrawable> _drawableObjects = null;
        private Dictionary<int, IDrawable> _internalDrawableObjects = null;

        private int width, height;
        private int[] mMatrixView;

        private Ponto2D iniPoint;

        private bool _interCreated = false;
        private bool _magnetIntersection = false;
        private Intersection _firstInter = null;
        private Intersection _firstInterLine = null;
        private Line _lineSelected = null;

        #endregion Fields

        #region Properties

        public bool InterCreated
        {
            get { return this._interCreated; }
            set { this._interCreated = value; }
        }

        public Dictionary<int, IDrawable> DrawableObjects
        {
            get { return this._drawableObjects; }
            set { this._drawableObjects = value; }
        }

        public Dictionary<int, IDrawable> InternalDrawableObjects
        {
            get { return this._internalDrawableObjects; }
            set { this._internalDrawableObjects = value; }
        }

        public bool MagnetIntersection
        {
            get { return this._magnetIntersection; }
            set { this._magnetIntersection = value; }
        }

        #endregion Properties

        #region Constructor

        public World(Dictionary<int, IDrawable> drawableObjects)
        {
            if (drawableObjects == null)
                throw new ArgumentNullException("drawableObjects");

            this.DrawableObjects = drawableObjects;
            this.InternalDrawableObjects = new Dictionary<int, IDrawable>();
            mMatrixView = new int[4];
            this.axes = new Axes();
            this.AddAxes();
        }

        #endregion Constructor

        #region Private Methods

        private void Polar(double anguloDegradiano, float distancia, Ponto2D pontoLine, ref Ponto2D pontoPolar)
        {
            double anguloRadiano = anguloDegradiano * Math.PI / 180;

            pontoPolar = pontoLine;

            pontoPolar.X += (float)Math.Round(Math.Cos(anguloRadiano) * distancia, 7);
            pontoPolar.Y += (float)Math.Round(Math.Sin(anguloRadiano) * distancia, 7);
        }

        private void AddAxes()
        {
            if (this.DrawableObjects != null)
                this.DrawableObjects.Add(axes.ID, axes);
        }

        private void SetViewportTransformation()
        {
            Gl.glViewport(0, 0, (int)width, (int)height);
        }

        private void SetProjectionTransformation(bool picking, Ponto2D ponto, int matrixHeight, int matrixWidth)
        {
            Gl.glMatrixMode(Gl.GL_PROJECTION);

            Gl.glLoadIdentity();
            if (picking)
            {
                int[] viewport = new int[4];
                //Gl.glGetIntegerv(Gl.GL_VIEWPORT, viewport);
                Glu.gluPickMatrix((double)ponto.X, (double)ponto.Y, matrixHeight, matrixWidth, mMatrixView);
            }

            //Glu.gluPerspective(45.0f, (float)width / (float)height, 0.5f, 500.0f);
            Gl.glOrtho(-width / 45.0, width / 45.0, -height / 45.0, height / 45.0, 0.5f, 500.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
        }

        private void UnProject(Ponto2D ponto, ref Ponto3D pontoSaida)
        {
            double[] mMatrixModel = new double[16];
            double[] mMatrixProjection = new double[16];

            Gl.glGetDoublev(Gl.GL_MODELVIEW_MATRIX, mMatrixModel);
            Gl.glGetDoublev(Gl.GL_PROJECTION_MATRIX, mMatrixProjection);
            Gl.glGetIntegerv(Gl.GL_VIEWPORT, mMatrixView);
            double x, y, z;
            Glu.gluUnProject(ponto.X, mMatrixView[3] - ponto.Y, 0, mMatrixModel, mMatrixProjection, mMatrixView, out x, out y, out z);
            pontoSaida.X = (float)x;
            pontoSaida.Y = (float)y;
            pontoSaida.Z = (float)z;
        }

        private void SelectLine(Line line)
        {
            if (line == null) return;

            this._lineSelected.LineColor = Color.Red;
            this._lineSelected.InitializePrimitives();
        }

        private void SelectIntersection(Intersection intersection)
        {
            if (intersection == null) return;

            intersection.ColorBorder = Color.Red;
            intersection.ColorInternal = Color.Green;
            intersection.InitializePrimitives();
        }

        private void AddLine(int code, Intersection inter)
        {
            Line line = new Line(this._firstInterLine.PanLocation, inter.PanLocation);

            line.InitializePrimitives();
            this.InternalDrawableObjects.Add(line.ID, line);
            line.InitialIntersection = new GroupingElements() { LineChangedPoint = ELinePoint.Initial, LineElement = this._firstInterLine };
            line.FinalIntersection = new GroupingElements() { LineChangedPoint = ELinePoint.Final, LineElement = inter };

            this._firstInterLine.InternalDraws.Add(line.ID, new GroupingElements() { LineChangedPoint = ELinePoint.Initial, LineElement = line });
            inter.InternalDraws.Add(line.ID, new GroupingElements() { LineChangedPoint = ELinePoint.Final, LineElement = line });
        }

        private void BreakLine(int code, Ponto2D ponto, Intersection inter)
        {
            if (this.InternalDrawableObjects.ContainsKey(code))
            {
                Line line = this.InternalDrawableObjects[code] as Line;
                line.PontoFinal = ponto;
                line.InitializePrimitives();
                Intersection lastIntersection = line.FinalIntersection.LineElement as Intersection;

                line.FinalIntersection.LineElement = inter;
                line.FinalIntersection.LineChangedPoint = ELinePoint.Final;

                Line newLine = new Line(ponto, lastIntersection.PanLocation);

                newLine.InitializePrimitives();
                this.InternalDrawableObjects.Add(newLine.ID, newLine);
                newLine.InitialIntersection.LineChangedPoint = ELinePoint.Initial;
                newLine.InitialIntersection.LineElement = inter;
                newLine.FinalIntersection.LineChangedPoint = ELinePoint.Final;
                newLine.FinalIntersection.LineElement = lastIntersection;

                lastIntersection.InternalDraws.Remove(line.ID);
                lastIntersection.InternalDraws.Add(newLine.ID, new GroupingElements() { LineChangedPoint = ELinePoint.Final, LineElement = newLine });

                inter.InternalDraws.Add(line.ID, new GroupingElements() { LineChangedPoint = ELinePoint.Final, LineElement = line });
                inter.InternalDraws.Add(newLine.ID, new GroupingElements() { LineChangedPoint = ELinePoint.Initial, LineElement = newLine });
            }
        }

        private double AnguloAbsolutoEntre(Line l1, Line l2)
        {
            //Vetor2D v1 = new Vetor2D(l1.FinalX - l1.InitialX, l1.FinalY - l1.InitialY);
            Vetor2D v1 = new Vetor2D(l1.PontoInicial, l1.PontoFinal);
            //Vetor2D v2 = new Vetor2D(l2.InitialX - l2.FinalX, l2.InitialY - l2.FinalY);
            Vetor2D v2 = new Vetor2D(l2.PontoInicial, l2.PontoFinal);
            //l1.InitializePrimitives();
            //l2.InitializePrimitives();
            double det = Vetor2D.Determinante(v1, v2);
            double cos = (v1 * v2) / (v1.Modulo() * v2.Modulo());
            double sign = Math.Sign(det) >= 0 ? 1 : -1;

            double angulo = sign * (Math.Acos(cos) * 180 / Math.PI);
            //angulo = 360 - angulo;

            if (angulo < 0 || det < 0)
            {
                if (angulo < 0)
                    angulo += 360;
                else
                    angulo += 180;
            }

            return angulo;
        }

        private int intersec2d(Line line1, Line line2)
        {
            double det;

            //det = (line2.FinalX - line2.InitialX) * (line1.FinalY - line1.InitialY) - (line2.FinalY - line2.InitialY) * (line1.FinalX - line1.InitialX);
            det = (line2.PontoFinal.X - line2.PontoInicial.X) * (line1.PontoFinal.Y - line1.PontoInicial.Y) -
                    (line2.PontoFinal.Y - line2.PontoInicial.Y) * (line1.PontoFinal.X - line1.PontoInicial.X);

            if (det == 0.0)
                return 0; // não há intersecção           

            //double s = ((line2.FinalX - line2.InitialX) * (line2.InitialY - line1.InitialY) - (line2.FinalY - line2.InitialY) * (line2.InitialX - line1.InitialX)) / det;
            //double t = ((line1.FinalX - line1.InitialX) * (line2.InitialY - line1.InitialY) - (line1.FinalY - line1.InitialY) * (line2.InitialX - line1.InitialX)) / det;

            return 1; // há intersecção
        }

        private void CreateIntersectionLine(Line line1, Line line2)
        {
            //Equacao da reta
            //y = ax + b
            //a = (y - y0) / (x - x0)
            //Ao obter a equacao da reta só igualar elas
            //Reta paralela ao eixo Y: x = constante
            //Reta paralela ao eixo X: Y = constante

            //Equacao primeira reta
            //float a1 = (line1.FinalY - (line1.InitialY)) / (line1.FinalX - (line1.InitialX));
            //float b1 = line1.InitialY - (a1 * line1.InitialX);
            float a1 = (line1.PontoFinal.Y - (line1.PontoInicial.Y)) / (line1.PontoFinal.X - (line1.PontoInicial.X));
            float b1 = line1.PontoInicial.Y - (a1 * line1.PontoInicial.X);

            //Equacao segunda reta
            //float a2 = (line2.FinalY - (line2.InitialY)) / (line2.FinalX - (line2.InitialX));
            //float b2 = line2.InitialY - (a2 * line2.InitialX);
            float a2 = (line2.PontoFinal.Y - (line2.PontoInicial.Y)) / (line2.PontoFinal.X - (line2.PontoInicial.Y));
            float b2 = line2.PontoInicial.Y - (a2 * line2.PontoInicial.X);

            string expressao1 = "y = " + a1 + " * x + " + b1;
            string expressao2 = "y = " + a2 + " * x + " + b2;

            float x = a1 - (a2);
            x = (b2 - (b1)) / x;
            float y = (a1 * x) + b1;

            Intersection inter = new Intersection(new Ponto2D() { X = x, Y = y });
            inter.InitializePrimitives();
            this.DrawableObjects.Add(inter.ID, inter);

            //Line l1 = new Line(line1.FinalX, line1.FinalY, inter.LocationX, inter.LocationY);
            Line l1 = new Line(line1.PontoFinal, inter.PanLocation);

            l1.InitializePrimitives();
            this.InternalDrawableObjects.Add(l1.ID, l1);

            //Line l2 = new Line(line2.FinalX, line2.FinalY, inter.LocationX, inter.LocationY);
            Line l2 = new Line(line2.PontoFinal, inter.PanLocation);

            l2.InitializePrimitives();
            this.InternalDrawableObjects.Add(l2.ID, l2);
            //As duas equacoes devem possuir X
        }

        private string WriteFile()
        {
            string dir = Directory.GetCurrentDirectory();
            dir = Path.Combine(dir, "polygon.plg");
            if (File.Exists(dir))
                File.Delete(dir);
            FileStream file = File.Create(dir);
            file.Close();

            using (StreamWriter stream = new StreamWriter(dir))
            {
                stream.Flush();
                stream.WriteLine("=Ambient=");

                //foreach (IDrawable draw in this.DrawableObjects.Values)
                //{
                //    draw.Draw(picking);
                //    //Desenhos internos, usar com cuidado pois o mesmo desenho pode estar em outras coleções internas! = Corrigido!
                //    //if (draw.InternalDraws != null && draw.InternalDraws.Count > 0)
                //    //{
                //    //    this.RecursiveDraws(draw.InternalDraws, picking);
                //    //}
                //}
                foreach (IDrawable draw in this.InternalDrawableObjects.Values)
                {
                    stream.WriteLine(draw.WritePoints());
                }
            }

            return dir;
        }

        private bool JoinIntersection(int id, Ponto2D ponto, int clientSize)
        {
            List<IDrawable> entities;
            int code = -1;
            entities = this.PickList(new Ponto2D() { X = ponto.X, Y = clientSize - ponto.Y - 1 }, 10, 10);
            if (entities.Count >= 2)
            {
                if (id == entities[0].ID)
                    code = entities[1].ID;
                else
                    code = entities[0].ID;
            }
            else
            {
                if (entities.Count == 1)
                    code = entities[0].ID;
            }
            if (code != -1 && code != id && this.DrawableObjects.ContainsKey(code) && this.DrawableObjects.ContainsKey(id))
            {
                Intersection inter = this.DrawableObjects[code] as Intersection;
                Intersection moveInter = this.DrawableObjects[id] as Intersection;
                foreach (GroupingElements line in moveInter.InternalDraws.Values)
                {
                    if (inter.InternalDraws.ContainsKey(line.LineElement.ID))
                    {
                        inter.InternalDraws.Remove(line.LineElement.ID);
                        this.InternalDrawableObjects.Remove(line.LineElement.ID);
                        continue;
                    }

                    inter.InternalDraws.Add(line.LineElement.ID, line);
                    Line l = line.LineElement as Line;
                    if (line.LineChangedPoint == ELinePoint.Final)
                    {
                        l.FinalIntersection.LineElement = inter;
                        l.PontoFinal = inter.PanLocation;
                    }
                    else if (line.LineChangedPoint == ELinePoint.Initial)
                    {
                        l.InitialIntersection.LineElement = inter;
                        l.PontoInicial = inter.PanLocation;
                    }

                    l.InitializePrimitives();
                }

                this.DrawableObjects.Remove(moveInter.ID);
                moveInter = null;
                return true;
            }

            return false;
        }

        #endregion Private Methods

        #region Internal Methods

        internal void InitGl()
        {
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            //Gl.glEnable(Gl.GL_DEPTH_TEST);
            //Gl.glEnable(Gl.GL_CULL_FACE);
        }

        internal void InitPrimitives()
        {
            foreach (IDrawable drawableObject in this.DrawableObjects.Values)
                drawableObject.InitializePrimitives();
        }

        internal void RenderScene()
        {
            this.RenderScene(false);
        }

        internal void RenderScene(bool picking)
        {
            this.RenderScene(picking, new Ponto2D() { X = 0.0f, Y = 0.0f });
        }

        internal void RenderScene(bool picking, Ponto2D ponto)
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT); //limpa buffer de cor e buffer de profundidade
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(0f, 0f, 15f, 0f, 0f, 0f, 0f, 1f, 0f);

            Gl.glPushMatrix();


            foreach (IDrawable draw in this.DrawableObjects.Values)
            {
                draw.Draw(picking);
            }

            foreach (IDrawable draw in this.InternalDrawableObjects.Values)
            {
                draw.Draw(picking);
            }

            if (_interCreated)
            {
                Ponto3D pontoSaida = new Ponto3D() { X = 0.0f, Y = 0.0f, Z = 0.0f };
                this.UnProject(ponto, ref pontoSaida);

                Gl.glPushMatrix();
                Gl.glColor3f(0f, 0f, 1f);
                Gl.glLoadName(this.GetHashCode());
                Gl.glBegin(Gl.GL_LINE_LOOP);
                Gl.glVertex2f(iniPoint.X, iniPoint.Y);
                Gl.glVertex2f(pontoSaida.X, pontoSaida.Y);

                Gl.glEnd();
                Gl.glPopMatrix();

                Gl.glPushMatrix();
                Gl.glLoadName(this.GetHashCode());
                Gl.glColor3f(0f, 1f, 0f);
                Gl.glBegin(Gl.GL_LINE_LOOP);

                Angulo angulo = Angulo.EntreDoisPontos(iniPoint.X, iniPoint.Y, pontoSaida.X, pontoSaida.Y);
                angulo.Degradianos += 90;
                Ponto2D pontoPolar = new Ponto2D() { X = 0.0f, Y = 0.0f };
                this.Polar(angulo.Degradianos, 0.5f, new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y }, ref pontoPolar);

                Gl.glVertex2f(pontoSaida.X, pontoSaida.Y);
                Gl.glVertex2f(pontoPolar.X, pontoPolar.Y);

                Gl.glEnd();
                Gl.glPopMatrix();

                Gl.glPushMatrix();
                Gl.glLoadName(this.GetHashCode());
                Gl.glColor3f(0f, 1f, 0f);
                Gl.glBegin(Gl.GL_LINE_LOOP);

                Ponto2D pontoPolarInicial = new Ponto2D() { X = 0.0f, Y = 0.0f };
                this.Polar(angulo.Degradianos, 0.5f, iniPoint, ref pontoPolarInicial);
                Gl.glVertex2f(iniPoint.X, iniPoint.Y);
                Gl.glVertex2f(pontoPolarInicial.X, pontoPolarInicial.Y);

                Gl.glEnd();
                Gl.glPopMatrix();

                Gl.glPushMatrix();

                Gl.glLoadName(this.GetHashCode());
                Gl.glColor3f(0f, 1f, 0f);
                Gl.glBegin(Gl.GL_LINE_STRIP);

                Gl.glVertex2f(pontoPolar.X, pontoPolar.Y);
                Gl.glVertex2f(pontoPolarInicial.X, pontoPolarInicial.Y);

                Gl.glEnd();
                Gl.glPopMatrix();
            }

            Gl.glPopMatrix();

            Gl.glFlush();

            int error = Gl.glGetError();
            if (error != 0)
                throw new ApplicationException("Erro: " + error.ToString());
        }

        internal void RecursiveDraws(Dictionary<int, GroupingElements> draws, bool picking)
        {
            foreach (var drawableObject in draws)
            {
                drawableObject.Value.LineElement.Draw(picking);
                if (drawableObject.Value.LineElement.InternalDraws != null && drawableObject.Value.LineElement.InternalDraws.Count > 0)
                {
                    this.RecursiveDraws(drawableObject.Value.LineElement.InternalDraws, picking);
                }
            }
        }

        internal void SetView(int height, int width)
        {
            this.width = width;
            this.height = height;

            mMatrixView[0] = 0;
            mMatrixView[1] = 0;
            mMatrixView[2] = width;
            mMatrixView[3] = height;

            Gl.glViewport(0, 0, width, height);

            Gl.glEnable(Gl.GL_CULL_FACE);

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            //Glu.gluPerspective(90.0f, (float)width / (float)height, 0.5f, 500.0f);
            Gl.glOrtho(-width / 45.0, width / 45.0, -height / 45.0, height / 45.0, 0.5f, 500.0f);

            Gl.glEnable(Gl.GL_DEPTH_TEST);                                  //elimina faces escondidas

            Gl.glEnable(Gl.GL_MULTISAMPLE_ARB);                             // Bloco -> aumenta antiliasing, qualidades das linhas
            Gl.glEnable(Gl.GL_LINE_SMOOTH);                                 //
            Gl.glEnable(Gl.GL_BLEND);                                       //
            Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);     //
            Gl.glHint(Gl.GL_LINE_SMOOTH_HINT, Gl.GL_DONT_CARE);             //
            Gl.glLineWidth(1.5f);                                           //

            Gl.glShadeModel(Gl.GL_SMOOTH);
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
            RenderScene();
        }

        internal void UpdateRotations(int id, Ponto2D ponto)
        {
            foreach (IDrawable drawableObject in this.DrawableObjects.Values)
                drawableObject.Rotate(ponto);
        }

        internal void UpdatePans(int id, Ponto2D ponto, int clientSize)
        {
            Ponto3D pontoSaida = new Ponto3D();
            this.UnProject(ponto, ref pontoSaida);

            if (this.DrawableObjects.ContainsKey(id))
            {
                if (this.MagnetIntersection)
                {
                    if (!this.JoinIntersection(id, ponto, clientSize))
                    {
                        this.DrawableObjects[id].Pan(new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y });
                    }
                }
                else
                {
                    this.DrawableObjects[id].Pan(new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y });
                }
            }
        }

        internal void UpdateZoom(int delta)
        {
            foreach (IDrawable drawableObject in this.DrawableObjects.Values)
                drawableObject.Zoom(delta);
        }

        internal int Pick(Ponto2D ponto)
        {
            return this.Pick(ponto, 1, 1);
        }

        internal int Pick(Ponto2D ponto, int matrixHeight, int matrixWidth)
        {
            const int bufferSize = 1000;
            uint[] buffer = new uint[bufferSize];
            Gl.glSelectBuffer(bufferSize, buffer);
            Gl.glRenderMode(Gl.GL_SELECT);
            Gl.glInitNames();
            Gl.glPushName(-1);
            SetViewportTransformation();
            SetProjectionTransformation(true, ponto, matrixHeight, matrixWidth);

            this.RenderScene(true);

            Gl.glPopName();

            int cont_hits = Gl.glRenderMode(Gl.GL_RENDER);
            int id = -1;

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            //Glu.gluPerspective(45.0f, (float)width / (float)height, 0.5f, 500.0f);
            Gl.glOrtho(-width / 45.0, width / 45.0, -height / 45.0, height / 45.0, 0.5f, 500.0f);

            List<IDrawable> entities = new List<IDrawable>();

            if (cont_hits > 0)
            {
                int i = 0;
                double zMin = 10.0;
                for (int j = 0; j < cont_hits; j++)
                {
                    double depthPixel = (double)buffer[i + 1] / 0xffffffff;
                    if (zMin > depthPixel)
                    {
                        zMin = depthPixel;
                        id = (int)buffer[i + 3];
                    }
                    i += 4;
                }
            }

            return id;
        }

        internal List<IDrawable> PickList(Ponto2D ponto, int matrixHeight, int matrixWidth)
        {
            const int bufferSize = 1000;
            uint[] buffer = new uint[bufferSize];
            Gl.glSelectBuffer(bufferSize, buffer);
            Gl.glRenderMode(Gl.GL_SELECT);
            Gl.glInitNames();
            Gl.glPushName(-1);
            SetViewportTransformation();
            SetProjectionTransformation(true, ponto, matrixHeight, matrixWidth);

            this.RenderScene(true);

            Gl.glPopName();

            int cont_hits = Gl.glRenderMode(Gl.GL_RENDER);
            int id = -1;

            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            //Glu.gluPerspective(45.0f, (float)width / (float)height, 0.5f, 500.0f);
            Gl.glOrtho(-width / 45.0, width / 45.0, -height / 45.0, height / 45.0, 0.5f, 500.0f);

            List<IDrawable> entities = new List<IDrawable>();

            float depthPixel = 0.0f;
            uint m = 0;

            for (uint i = 0; i < cont_hits; i++)
            {
                uint z = buffer[m + 1];
                int hashCode = (int)buffer[m + 3];
                depthPixel = (float)buffer[m + 1] / 0xffffffff;

                //if ( m_sceneMgr.GetEntity( (int)hashCode ) == null ) continue;

                IDrawable entidade = null;
                if (this.DrawableObjects.ContainsKey(hashCode))
                {
                    entidade = this.DrawableObjects[hashCode];
                }
                else if (this.InternalDrawableObjects.ContainsKey(hashCode))
                {
                    entidade = this.InternalDrawableObjects[hashCode];
                }

                //int posicao = PickEntidade_GetPosicaoEntidade(z, zPosicoes);
                //entidades.Insert(posicao, entidade);
                //zPosicoes.Insert(posicao, z);
                if (entidade != null)
                    entities.Add(entidade);
                m += buffer[m] + 3;
            }

            return entities;
        }

        internal void CreateIntersection(Ponto2D ponto, int clientSize)
        {
            Ponto3D pontoSaida = new Ponto3D() { X = 0.0f, Y = 0.0f, Z = 0.0f };
            this.UnProject(ponto, ref pontoSaida);

            int code = -1;
            code = this.Pick(new Ponto2D() { X = ponto.X, Y = clientSize - ponto.Y - 1 }, 10, 10);
            if (this.DrawableObjects.ContainsKey(code) && this._firstInter != null && this._firstInter.ID != code && this.InterCreated)
            {
                Intersection i = this.DrawableObjects[code] as Intersection;
                Line line = new Line(new Ponto2D() { X = iniPoint.X, Y = iniPoint.Y }, new Ponto2D() { X = i.PanLocation.X, Y = i.PanLocation.Y });

                line.InitializePrimitives();
                this.InternalDrawableObjects.Add(line.ID, line);

                i.InternalDraws.Add(line.ID, new GroupingElements() { LineChangedPoint = ELinePoint.Final, LineElement = line });

                if (_firstInter.InternalDraws != null)
                {
                    GroupingElements group = new GroupingElements() { LineElement = line, LineChangedPoint = ELinePoint.Initial };
                    _firstInter.InternalDraws.Add(line.ID, group);
                    _firstInter.PointLine = new Ponto2D() { X = iniPoint.X, Y = iniPoint.Y };
                }

                line.InitialIntersection = new GroupingElements() { LineElement = _firstInter, LineChangedPoint = ELinePoint.Initial };
                line.FinalIntersection = new GroupingElements() { LineElement = i, LineChangedPoint = ELinePoint.Final };

                _interCreated = false;
                return;
            }

            Intersection inter = new Intersection(new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y });
            inter.InitializePrimitives();
            this.DrawableObjects.Add(inter.ID, inter);

            if (_interCreated)
            {
                Line line = new Line(new Ponto2D() { X = iniPoint.X, Y = iniPoint.Y }, new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y });
                line.InitializePrimitives();
                this.InternalDrawableObjects.Add(line.ID, line);
                //this.Polar(90, 5f, line);
                if (_firstInter.InternalDraws != null)
                {
                    GroupingElements group = new GroupingElements() { LineElement = line, LineChangedPoint = ELinePoint.Initial };
                    _firstInter.InternalDraws.Add(line.ID, group);
                    _firstInter.PointLine = new Ponto2D() { X = iniPoint.X, Y = iniPoint.Y };
                }

                if (inter.InternalDraws != null)
                {
                    GroupingElements group = new GroupingElements() { LineElement = line, LineChangedPoint = ELinePoint.Final };
                    inter.InternalDraws.Add(line.ID, group);
                    inter.PointLine = new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y };
                }

                line.InitialIntersection = new GroupingElements() { LineElement = _firstInter, LineChangedPoint = ELinePoint.Initial };
                line.FinalIntersection = new GroupingElements() { LineElement = inter, LineChangedPoint = ELinePoint.Final };
            }
            _firstInter = inter;
            iniPoint.X = pontoSaida.X;
            iniPoint.Y = pontoSaida.Y;
            _interCreated = true;

            if (this.InternalDrawableObjects.ContainsKey(code))
            {
                BreakLine(code, new Ponto2D() { X = pontoSaida.X, Y = pontoSaida.Y }, inter);
            }
        }

        //public static double AnguloEntre(Line l1, Line l2)
        //{
        //    Vetor2D v1 = new Vetor2D(l1.FinalX - l1.InitialX, l1.FinalY - l1.InitialY);
        //    Vetor2D v2 = new Vetor2D(l2.InitialX - l2.FinalX, l2.InitialY - l2.FinalY);
        //    //l1.InitializePrimitives();
        //    //l2.InitializePrimitives();
        //    double det = Vetor2D.Determinante(v1, v2);
        //    double cos = (v1 * v2) / (v1.Modulo() * v2.Modulo());
        //    double sign = Math.Sign(det) >= 0 ? 1 : -1;

        //    double ata = Math.Atan2(v2.m_a, v2.m_b);
        //    //double angulo2 = sign * (Math.Acos(ata) * 180 / Math.PI);
        //    double angulo = Math.Acos(cos);
        //    //angulo = 360 - angulo;

        //    //if (angulo < 0 || det < 0)
        //    //{
        //    //    if (angulo < 0)
        //    //        angulo += 360;
        //    //    else
        //    //        angulo += 180;
        //    //}

        //    return angulo;
        //}

        //public static double AnguloRelativoEntre(Line l1, Line l2)
        //{
        //    Vetor2D v1 = new Vetor2D(l1.FinalX - l1.InitialX, l1.FinalY - l1.InitialY);
        //    Vetor2D v2 = new Vetor2D(l2.FinalX - l2.InitialX, l2.FinalY - l2.InitialY);

        //    double det = Vetor2D.Determinante(v1, v2);
        //    double cos = ((v1 * v2) / (v1.Modulo() * v2.Modulo()));
        //    double sign = Math.Sign(det) >= 0 ? 1 : -1;

        //    double angulo = sign * (Math.Acos(cos) * 180 / Math.PI);

        //    return angulo;
        //}

        internal void ClearInterLine()
        {
            if (this._firstInterLine != null)
            {
                this._firstInterLine.ColorBorder = Color.Blue;
                this._firstInterLine.ColorInternal = Color.White;
                this._firstInterLine.InitializePrimitives();
            }
            this._firstInterLine = null;
        }

        internal void ClearSelectedLine()
        {
            if (this._lineSelected != null)
            {
                this._lineSelected.LineColor = Color.Blue;
                this._lineSelected.InitializePrimitives();
            }
            this._lineSelected = null;
        }

        internal void CreateLine(Ponto2D ponto, int clientSize)
        {
            int code = -1;
            code = this.Pick(new Ponto2D() { X = ponto.X, Y = clientSize - ponto.Y - 1 }, 10, 10);
            if (code == -1)
            {
                this.ClearInterLine();
                return;
            }
            if (this._firstInterLine == null)
            {
                if (!this.DrawableObjects.ContainsKey(code)) return;

                this._firstInterLine = this.DrawableObjects[code] as Intersection;

                int id1 = -1;
                int id2 = -1;
                int cont = 0;
                foreach (var item in this._firstInterLine.InternalDraws.Values)
                {
                    if (cont == 2) break;

                    if (cont == 0)
                        id1 = item.LineElement.ID;
                    if (cont == 1)
                        id2 = item.LineElement.ID;

                    cont++;
                }
                //double teste = AnguloAbsolutoEntre(this._firstInterLine.InternalDraws[id1].LineElement as Line,
                //    this._firstInterLine.InternalDraws[id2].LineElement as Line);

                //double ang = AnguloEntre(this._firstInterLine.InternalDraws[id1].LineElement as Line,
                //    this._firstInterLine.InternalDraws[id2].LineElement as Line);

                //Gl.glPushMatrix();
                //Gl.glTranslatef(this._firstInterLine.LocationX, this._firstInterLine.LocationY, 0);
                //Gl.glLoadName(this.GetHashCode());
                //float circle_points = 50.0f;
                //float angle, raioX = 2.0f, raioY = 2.0f;
                //Gl.glBegin(Gl.GL_LINE_LOOP);
                //for (int i = 0; i < circle_points; i++)
                //{
                //    angle = (float)(1 * Math.PI * i / circle_points);
                //    Gl.glVertex2f((float)Math.Cos(angle) * raioX, (float)Math.Sin(angle) * raioY);
                //}
                //Gl.glEnd();               

                //Gl.glPopMatrix();

                //double teste1 = AnguloRelativoEntre(this._firstInterLine.InternalDraws[9035653].LineElement as Line,
                //    this._firstInterLine.InternalDraws[22330821].LineElement as Line);

                this.SelectIntersection(this._firstInterLine);
            }
            else
            {
                Intersection secondInterLine = this.DrawableObjects[code] as Intersection;
                if (secondInterLine != null)
                {
                    Ponto3D pontoUnproject = new Ponto3D() { X = 0.0f, Y = 0.0f, Z = 0.0f };
                    this.UnProject(ponto, ref pontoUnproject);
                    this.AddLine(code, secondInterLine);

                    this.ClearInterLine();
                }
            }
        }

        internal bool SelectDrawableObject(Ponto2D ponto, int clientSize)
        {
            this.ClearSelectedLine();
            this.ClearInterLine();
            int code = -1;
            code = this.Pick(new Ponto2D() { X = ponto.X, Y = clientSize - ponto.Y - 1 }, 10, 10);
            if (code == -1)
            {
                return false;
            }

            if (!this.InternalDrawableObjects.ContainsKey(code) &&
                !this.DrawableObjects.ContainsKey(code)) return false;

            //Line line2 = this.InternalDrawableObjects[code] as Line;
            //line2.InitializePrimitives();
            //this.intersec2d(this._lineSelected, line2);
            //this.CreateIntersectionLine(this._lineSelected, line2);
            //this._lineSelected = this.InternalDrawableObjects[code] as Line;
            //this._lineSelected.InitializePrimitives();
            if (this.InternalDrawableObjects.ContainsKey(code))
            {
                this._lineSelected = this.InternalDrawableObjects[code] as Line;
                this.SelectLine(this._lineSelected);
                return true;
            }
            else
            {
                this._firstInterLine = this.DrawableObjects[code] as Intersection;
                this.SelectIntersection(this._firstInterLine);
                return true;
            }
        }

        internal void DeleteSelectedObject()
        {
            if (this._lineSelected != null)
            {
                Intersection intersection = this._lineSelected.InitialIntersection.LineElement as Intersection;

                intersection.InternalDraws.Remove(this._lineSelected.ID);

                intersection = this._lineSelected.FinalIntersection.LineElement as Intersection;

                intersection.InternalDraws.Remove(this._lineSelected.ID);
                this.InternalDrawableObjects.Remove(this._lineSelected.ID);
                this.ClearSelectedLine();
            }
            else if (this._firstInterLine != null)
            {
                foreach (var items in this._firstInterLine.InternalDraws)
                {
                    if (items.Value.LineElement is Line)
                    {
                        Line line = items.Value.LineElement as Line;
                        if (items.Value.LineChangedPoint == ELinePoint.Final)
                        {
                            Intersection intersection = line.InitialIntersection.LineElement as Intersection;
                            intersection.InternalDraws.Remove(line.ID);
                        }
                        else
                        {
                            Intersection intersection = line.FinalIntersection.LineElement as Intersection;
                            intersection.InternalDraws.Remove(line.ID);
                        }
                        this.InternalDrawableObjects.Remove(line.ID);
                    }
                }
                this._firstInterLine.InternalDraws.Clear();
                this.DrawableObjects.Remove(this._firstInterLine.ID);
                this.ClearInterLine();
            }
        }

        internal void ClearViewPort()
        {
            this._interCreated = false;
            this.ClearInterLine();
            this.ClearSelectedLine();
            this.InternalDrawableObjects.Clear();
            this.DrawableObjects.Clear();
            this.AddAxes();
            this.RenderScene();
        }

        internal string Build3D()
        {
            return this.WriteFile();
        }

        #endregion Internal Methods
    }
}