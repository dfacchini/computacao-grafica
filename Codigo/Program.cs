﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using openGl_Control.Elements;

namespace openGl_Control
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            
            //    //Polygon polygon = new Polygon();
            //    //Cube cube = new Cube();
            //    PolygonShape shape = new PolygonShape();
            //    List<IDrawable> drawableObjects = new List<IDrawable>();

            //    //drawableObjects.Add(polygon);
            //    drawableObjects.Add(axes);
            //    drawableObjects.Add(shape);

            //    World world = new World(drawableObjects);
            ////    Application.EnableVisualStyles();
            ////    Application.SetCompatibleTextRenderingDefault(false);

            ////    Application.Run(new MainOpenglControl(world, polygon.CalculateArea(), polygon));
            //Application.Run(new MainOpenglControl(world, axes));

            //Wall wall = new Wall(100, 100);
            //SimpleMovimentation shape = new SimpleMovimentation();
            Dictionary<int, IDrawable> drawableObjects = new Dictionary<int, IDrawable>();
            //drawableObjects.Add(wall);            
            //drawableObjects.Add(shape);

            World world = new World(drawableObjects);
            Application.Run(new Project_OpenglControl(world));
        }
    }
}