﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using openGl_Control.Elements;
using Tao.OpenGl;

namespace openGl_Control
{
    public class World3D
    {
        #region Properties

        public List<IDrawable> drawableObjects = null;

        #endregion Properties

        #region Constructor

        public World3D()
        {
            this.drawableObjects = new List<IDrawable>();
            //this.drawableObjects.Add(new SimpleMovimentation());
        }

        #endregion Constructor

        #region Internal Methods

        internal void InitGl()
        {
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_CULL_FACE);
        }

        internal void InitPrimitives()
        {
            foreach (IDrawable drawableObject in this.drawableObjects)
                drawableObject.InitializePrimitives();
        }

        internal void RenderScene()
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(0f, 0f, 50f, 0f, 0f, 0f, 0f, 1f, 0f);

            Gl.glPushMatrix();
            foreach (IDrawable drawableObject in this.drawableObjects)
                drawableObject.Draw();
            Gl.glPopMatrix();

            Gl.glFlush();

            int error = Gl.glGetError();
            if (error != 0)
                throw new ApplicationException("An error has occurred: " + error.ToString());
        }

        internal void SetView(int height, int width)
        {
            Gl.glViewport(0, 0, width, height);

            Gl.glEnable(Gl.GL_CULL_FACE);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(45.0f, (float)width / (float)height, 5f, 500.0f);

            Gl.glEnable(Gl.GL_DEPTH_TEST);

            Gl.glEnable(Gl.GL_MULTISAMPLE_ARB);                             // aumenta antiliasing, qualidades das linhas
            Gl.glEnable(Gl.GL_LINE_SMOOTH);                                 //
            Gl.glEnable(Gl.GL_BLEND);                                       //
            Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);     //
            Gl.glHint(Gl.GL_LINE_SMOOTH_HINT, Gl.GL_NICEST);                //
            Gl.glLineWidth(2.5f);                                           //

            Gl.glShadeModel(Gl.GL_SMOOTH);
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            RenderScene();
        }

        internal void UpdateLocations(float x, float y)
        {
            foreach (IDrawable drawableObject in this.drawableObjects)
                if (drawableObject != null)
                    drawableObject.Rotate(new Ponto2D() { X = x, Y = y });
        }

        internal void UpdatePans(Ponto2D ponto)
        {
            foreach (IDrawable draw in this.drawableObjects)
                draw.Pan(ponto);
        }

        internal void UpdateZoom(int delta)
        {
            foreach (IDrawable drawableObject in this.drawableObjects)
                drawableObject.Zoom(delta);
        }

        #endregion Internal Methods
    }
}
