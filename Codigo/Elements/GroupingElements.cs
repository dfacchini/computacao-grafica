﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using openGl_Control.Elements.Enums;

namespace openGl_Control.Elements
{
    public class GroupingElements
    {
        #region Fields

        private ELinePoint _linePointElement = ELinePoint.Whithout;
        private IDrawable _lineElement = null;

        #endregion Fields

        public ELinePoint LineChangedPoint
        {
            get { return this._linePointElement; }
            set { this._linePointElement = value; }
        }

        public IDrawable LineElement
        {
            get { return this._lineElement; }
            set { this._lineElement = value; }
        }

        #region Constructor

        public GroupingElements()
        {
        }

        #endregion Constructor
    }
}