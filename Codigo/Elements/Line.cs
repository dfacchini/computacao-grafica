﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class Line : IDrawable
    {
        #region Fields

        private int m_list;
        private int m_id;

        private Ponto2D _axesLocation;
        private Ponto2D _panLocation;

        private Ponto2D _pontoInicial;
        private Ponto2D _pontoFinal;

        private GroupingElements initialIntersection = null;
        private GroupingElements finalIntersection = null;

        private Color _lineColor = Color.Blue;
        private bool _showWidthInitial = true;
        private bool _showWidthFinal = true;

        #endregion Fields

        #region Properties

        public GroupingElements InitialIntersection
        {
            get { return this.initialIntersection; }
            set { this.initialIntersection = value; }
        }

        public GroupingElements FinalIntersection
        {
            get { return this.finalIntersection; }
            set { this.finalIntersection = value; }
        }

        public int ID
        {
            get { return this.m_id; }
            set { this.m_id = value; }
        }

        public Ponto2D PontoInicial
        {
            get { return this._pontoInicial; }
            set { this._pontoInicial = value; }
        }

        public Ponto2D PontoFinal
        {
            get { return this._pontoFinal; }
            set { this._pontoFinal = value; }
        }

        public Color LineColor
        {
            get { return this._lineColor; }
            set { this._lineColor = value; }
        }

        public bool ShowWidthInitial
        {
            get { return this._showWidthInitial; }
            set { this._showWidthInitial = value; }
        }

        public bool ShowWidthFinal
        {
            get { return this._showWidthFinal; }
            set { this._showWidthFinal = value; }
        }

        #endregion Properties

        #region Constructor

        public Line(Ponto2D pontoInicial, Ponto2D pontoFinal)
        {
            this.PontoInicial = pontoInicial;
            this.PontoFinal = pontoFinal;
            this.ID = this.GetHashCode();
            this.initialIntersection = new GroupingElements();
            this.finalIntersection = new GroupingElements();
        }

        #endregion Constructor

        #region Private Methods

        private void Polar(double anguloDegradiano, float distancia, Ponto2D pontoLine, ref Ponto2D pontoPolar)
        {
            double anguloRadiano = anguloDegradiano * Math.PI / 180;

            pontoPolar = pontoLine;

            pontoPolar.X += (float)Math.Round(Math.Cos(anguloRadiano) * distancia, 7);
            pontoPolar.Y += (float)Math.Round(Math.Sin(anguloRadiano) * distancia, 7);
        }

        #endregion

        #region IDrawable

        public void Draw()
        {
            this.Draw(false);
        }

        public void Draw(bool picking)
        {
            Gl.glPushMatrix();

            if (picking)
                Gl.glLoadName(this.ID);

            Gl.glCallList(m_list);

            Gl.glPopMatrix();
        }

        public void InitializePrimitives()
        {
            m_list = Gl.glGenLists(1);
            Gl.glNewList(m_list, Gl.GL_COMPILE);

            Gl.glPushMatrix();
            Gl.glColor3f(this.LineColor.R, this.LineColor.G, this.LineColor.B);

            Gl.glBegin(Gl.GL_LINE_LOOP);
            Gl.glVertex2f(this.PontoInicial.X, this.PontoInicial.Y);
            Gl.glVertex2f(this.PontoFinal.X, this.PontoFinal.Y);

            Gl.glEnd();
            Gl.glPopMatrix();

            ////////////////////////////////////

            Angulo angulo = Angulo.EntreDoisPontos(PontoInicial.X, PontoInicial.Y, PontoFinal.X, PontoFinal.Y);
            angulo.Degradianos += 90;
            Ponto2D pontoPolar = new Ponto2D() { X = 0.0f, Y = 0.0f };
            this.Polar(angulo.Degradianos, 0.5f, new Ponto2D() { X = PontoFinal.X, Y = PontoFinal.Y }, ref pontoPolar);

            if (this.ShowWidthFinal)
            {
                Gl.glPushMatrix();
                Gl.glColor3f(0f, 1f, 0f);
                Gl.glBegin(Gl.GL_LINE_LOOP);

                Gl.glVertex2f(PontoFinal.X, PontoFinal.Y);
                Gl.glVertex2f(pontoPolar.X, pontoPolar.Y);

                Gl.glEnd();
                Gl.glPopMatrix();
            }

            Ponto2D pontoPolarInicial = new Ponto2D() { X = 0.0f, Y = 0.0f };
            this.Polar(angulo.Degradianos, 0.5f, PontoInicial, ref pontoPolarInicial);

            if (this.ShowWidthInitial)
            {
                Gl.glPushMatrix();
                Gl.glColor3f(0f, 1f, 0f);
                Gl.glBegin(Gl.GL_LINE_LOOP);

                Gl.glVertex2f(PontoInicial.X, PontoInicial.Y);
                Gl.glVertex2f(pontoPolarInicial.X, pontoPolarInicial.Y);

                Gl.glEnd();
                Gl.glPopMatrix();
            }

            Gl.glPushMatrix();
            Gl.glColor3f(0f, 1f, 0f);
            Gl.glBegin(Gl.GL_LINE_STRIP);

            Gl.glVertex2f(pontoPolar.X, pontoPolar.Y);
            Gl.glVertex2f(pontoPolarInicial.X, pontoPolarInicial.Y);

            Gl.glEnd();
            Gl.glPopMatrix();
            ///////////////////////////////////////////////////////////////////

            Gl.glEndList();
        }

        public void Rotate(Ponto2D ponto)
        {
            this._axesLocation = ponto;
        }

        public void Pan(Ponto2D ponto)
        {
            this._panLocation = ponto;
        }

        public void Zoom(int delta)
        {
        }

        #endregion IDrawable

        #region Public Methods

        public void IntersectionLocatinChanged(Ponto2D ponto, Ponto2D newPonto)
        {
            float tempX, tempX2, temY, tempY2;
            tempX = this.PontoInicial.X - ponto.X;
            temY = this.PontoInicial.Y - ponto.Y;

            tempX2 = this.PontoFinal.X - ponto.X;
            tempY2 = this.PontoFinal.Y - ponto.Y;

            if (tempX + temY < tempX2 + tempY2)
            {
                this.PontoInicial = new Ponto2D() { X = newPonto.X, Y = newPonto.Y };
                this.InitializePrimitives();
            }
            else
            {
                this.PontoFinal = new Ponto2D() { X = newPonto.X, Y = newPonto.Y };
                this.InitializePrimitives();
            }
        }

        public Dictionary<int, GroupingElements> InternalDraws
        {
            get
            {
                return null;
            }
            set { }
        }

        public string WritePoints()
        {
            string line = "PI ";
            line += PontoInicial.X + " " + PontoInicial.Y + "\r\n";
            line += "PF ";
            line += PontoFinal.X + " " + PontoFinal.Y + "\r\n";
            return line;
        }

        #endregion
    }
}