﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using openGl_Control.Elements.Enums;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class Intersection : IDrawable
    {
        #region Fields

        private int m_list;
        private int m_id;

        private Ponto2D _axesLocation;
        private Ponto2D _pointLine;
        private Ponto2D _panLocation;

        private Dictionary<int, GroupingElements> _internalDraws = null;
        private Color _colorBorder = Color.Blue;
        private Color _colorInternal = Color.White;

        #endregion Fields

        #region Properties

        public Ponto2D PanLocation
        {
            get { return this._panLocation; }
            private set { this._panLocation = value; }
        }

        public int ID
        {
            get { return this.m_id; }
            set { this.m_id = value; }
        }

        public Ponto2D PointLine
        {
            get { return this._pointLine; }
            set { this._pointLine = value; }
        }

        public Color ColorBorder
        {
            get { return this._colorBorder; }
            set { this._colorBorder = value; }
        }

        public Color ColorInternal
        {
            get { return this._colorInternal; }
            set { this._colorInternal = value; }
        }

        #endregion Properties

        #region Constructor

        public Intersection(Ponto2D ponto)
        {
            this.PanLocation = ponto;
            this._internalDraws = new Dictionary<int, GroupingElements>();
            this.ID = this.GetHashCode();
        }

        #endregion Constructor

        #region Private Methods

        private void TranslateLines()
        {
            if (this.InternalDraws == null) return;

            //Temporario
            foreach (var draw in this.InternalDraws)
            {
                Line line = draw.Value.LineElement as Line;
                if (draw.Value.LineChangedPoint == ELinePoint.Final)
                {
                    line.PontoFinal = this.PanLocation;
                    line.InitializePrimitives();
                }
                else
                {
                    line.PontoInicial = this.PanLocation;
                    line.InitializePrimitives();
                }
            }
        }

        #endregion Private Methods

        #region IDrawable

        public Dictionary<int, GroupingElements> InternalDraws
        {
            get
            {
                return this._internalDraws;
            }
            set
            {
                this._internalDraws = value;
            }
        }

        public void Draw()
        {
            this.Draw(false);
        }

        public void Draw(bool picking)
        {
            Gl.glPushMatrix();

            if (picking)
                Gl.glLoadName(this.ID);

            Gl.glTranslatef(this.PanLocation.X, this.PanLocation.Y, 0.0f);

            Gl.glRotatef(this._axesLocation.X, 1f, 0f, 0f);
            Gl.glRotatef(this._axesLocation.Y, 0f, 1f, 0f);

            Gl.glCallList(m_list);

            Gl.glPopMatrix();
        }

        public void InitializePrimitives()
        {
            m_list = Gl.glGenLists(1);
            Gl.glNewList(m_list, Gl.GL_COMPILE);

            Gl.glPushMatrix();

            Gl.glColor3f(this._colorInternal.R, this._colorInternal.G, this._colorInternal.B);
            Gl.glBegin(Gl.GL_POLYGON);
            Gl.glVertex3f(-0.2f, -0.2f, 0.2f);
            Gl.glVertex3f(0.2f, -0.2f, 0.2f);
            Gl.glVertex3f(0.2f, 0.2f, 0.2f);
            Gl.glVertex3f(-0.2f, 0.2f, 0.2f);
            Gl.glEnd();

            Gl.glPopMatrix();

            Gl.glPushMatrix();

            Gl.glColor3f(this._colorBorder.R, this._colorBorder.G, this._colorBorder.B);
            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.25f, 0.25f, 0.2f);
            Gl.glVertex3f(0.25f, 0.25f, 0.2f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.25f, -0.25f, 0.2f);
            Gl.glVertex3f(0.25f, -0.25f, 0.2f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(0.25f, -0.25f, 0.2f);
            Gl.glVertex3f(0.25f, 0.25f, 0.2f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.25f, -0.25f, 0.2f);
            Gl.glVertex3f(-0.25f, 0.25f, 0.2f);
            Gl.glEnd();

            Gl.glPopMatrix();

            Gl.glEndList();
        }

        public void Rotate(Ponto2D ponto)
        {
            this._axesLocation = ponto;
        }

        public void Pan(Ponto2D ponto)
        {
            this.PanLocation = ponto;
            this.TranslateLines();
        }

        public void Zoom(int delta)
        {
        }

        #endregion IDrawable

        public string WritePoints()
        {
            return null;
        }
    }
}