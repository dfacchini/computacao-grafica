﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class SimpleMovimentation : IDrawable
    {
        #region Consts

        private int m_list;

        #endregion Consts

        #region Variable and Properties

        private Ponto2D location;
        private Ponto2D panLocation;

        private float axesZTranslated;

        private List<Ponto3D> pontos;
        private List<Ponto3D> pontosB;
        private List<Ponto3D> height;

        #endregion Variable and Properties

        #region Constructor

        public SimpleMovimentation(List<Ponto3D> ponto2, List<Ponto3D> pontoB2, List<Ponto3D> inter)
        {
            pontos = ponto2;
            pontosB = pontoB2;
            height = inter;
        }

        #endregion Constructor

        #region IDrawable

        public void Draw()
        {
            this.Draw(false);
        }

        public void Draw(bool picking)
        {
            #region Movement Axes

            Gl.glPushMatrix();

            if (picking)
                Gl.glLoadName(this.GetHashCode());

            Gl.glTranslatef(this.panLocation.X, this.panLocation.Y, axesZTranslated);

            Gl.glRotatef(this.location.X, 1f, 0f, 0f);
            Gl.glRotatef(this.location.Y, 0f, 1f, 0f);

            Gl.glCallList(m_list);

            Gl.glPopMatrix();

            #endregion Movement Axes
        }

        public void Zoom(int delta)
        {
            if (delta > 0)
                axesZTranslated += 1f;
            else
                axesZTranslated -= 1f;
        }

        public void InitializePrimitives()
        {
            m_list = Gl.glGenLists(1);
            Gl.glNewList(m_list, Gl.GL_COMPILE);
            if (this.pontos.Count == 0) return;

            #region Floor

            float maiorX = pontos[0].X;
            float maiorZ = pontos[0].Y;

            float maiorNegX = pontos[0].X;
            float maiorNegZ = pontos[0].Y;

            foreach (Ponto3D ponto in this.pontos)
            {
                if (ponto.X > maiorX)
                    maiorX = ponto.X;
                if (ponto.Z > maiorZ)
                    maiorZ = ponto.Z;

                if (ponto.X < maiorNegX)
                    maiorNegX = ponto.X;
                if (ponto.Z < maiorNegZ)
                    maiorNegZ = ponto.Z;
            }

            Gl.glPushMatrix();
            Gl.glColor3f(0.0f, 0.6f, 0.4f);
            Gl.glBegin(Gl.GL_QUADS);

            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorZ + 5.0f);
            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorZ + 5.0f);

            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorZ + 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorZ + 5.0f);

            Gl.glEnd();

            Gl.glColor3f(0.0f, 0.8f, 0.6f);

            Gl.glBegin(Gl.GL_LINES);

            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorZ + 5.0f);
            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorZ + 5.0f);

            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorZ + 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorZ + 5.0f);
            Gl.glVertex3f(maiorX + 5.0f, -2.2f, maiorNegZ - 5.0f);
            Gl.glVertex3f(maiorNegX - 5.0f, -2.2f, maiorNegZ - 5.0f);

            Gl.glEnd();

            Gl.glPopMatrix();

            #endregion

            #region Faces

            Gl.glPushMatrix();
            Gl.glColor3f(0.0f, 0.5f, 1.0f);
            Gl.glBegin(Gl.GL_QUADS);

            for (int i = 0; i < this.pontos.Count; i += 2)
            {
                if (i + 1 == this.pontos.Count) break;

                Gl.glVertex3f(pontosB[i + 1].X, pontosB[i + 1].Y, pontosB[i + 1].Z);
                Gl.glVertex3f(pontos[i + 1].X, pontos[i + 1].Y, pontos[i + 1].Z);
                Gl.glVertex3f(pontos[i].X, pontos[i].Y, pontos[i].Z);
                Gl.glVertex3f(pontosB[i].X, pontosB[i].Y, pontosB[i].Z);

                Gl.glVertex3f(pontosB[i].X, pontosB[i].Y, pontosB[i].Z);
                Gl.glVertex3f(pontos[i].X, pontos[i].Y, pontos[i].Z);
                Gl.glVertex3f(pontos[i + 1].X, pontos[i + 1].Y, pontos[i + 1].Z);
                Gl.glVertex3f(pontosB[i + 1].X, pontosB[i + 1].Y, pontosB[i + 1].Z);
            }

            Gl.glEnd();
            Gl.glPopMatrix();

            #endregion

            #region Lines

            Gl.glPushMatrix();

            Gl.glColor3f(1.0f, 1.0f, 1.0f);
            Gl.glBegin(Gl.GL_LINES);
            foreach (Ponto3D pnt in pontos)
            {
                Gl.glVertex3f(pnt.X, pnt.Y, pnt.Z);
            }
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            foreach (Ponto3D pnt in pontosB)
            {
                Gl.glVertex3f(pnt.X, pnt.Y, pnt.Z);
            }
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            foreach (Ponto3D pnt in height)
            {
                Gl.glVertex3f(pnt.X, pnt.Y, pnt.Z);
            }
            Gl.glEnd();

            Gl.glPopMatrix();

            #endregion

            Gl.glEndList();
        }

        public Dictionary<int, GroupingElements> InternalDraws
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int ID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Rotate(Ponto2D ponto)
        {
            this.location = ponto;
        }

        public void Pan(Ponto2D ponto)
        {
            this.panLocation = ponto;
        }

        public string WritePoints()
        {
            throw new NotImplementedException();
        }

        #endregion IDrawable
    }
}