﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control.Elements.Enums
{
    [Flags]
    public enum EMode
    {
        None = 1,
        Construction = 2,
        AddLine = 4,        
        SelectObject = 8
    }
}