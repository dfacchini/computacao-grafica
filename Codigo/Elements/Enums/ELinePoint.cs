﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control.Elements.Enums
{
    [Flags]
    public enum ELinePoint
    {
        Whithout = 1,
        Initial = 2,
        Final = 4
    }
}