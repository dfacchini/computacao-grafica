﻿using System;
using System.Collections.Generic;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class Axes : IDrawable
    {
        #region Consts

        private int m_list;

        #endregion Consts

        #region Variable and Properties

        private float axesXLocation;
        private float axesYLocation;
        private float axesZLocation;
        private int m_id;

        private float axesZTranslated;

        private float axisSize;

        public float AxisSize
        {
            get { return this.axisSize; }
            private set { this.axisSize = value; }
        }

        #endregion Variable and Properties

        #region Constructor

        public Axes()
        {
            this.axisSize = 13.0f;
            this.ID = this.GetHashCode();
        }

        #endregion Constructor

        #region Public Methods

        public void ReCalculateArea(float newArea)
        {
            this.AxisSize = newArea;

            this.InitializePrimitives();
        }

        #endregion Public Methods

        #region IDrawable

        public void Draw()
        {
            this.Draw(false);
        }

        public void Rotate(Ponto2D ponto)
        {
            this.axesXLocation = ponto.X;
            this.axesYLocation = ponto.Y;
        }

        public void Zoom(int delta)
        {
            if (delta > 0)
                axesZTranslated += 1f;
            else
                axesZTranslated -= 1f;
        }

        public void InitializePrimitives()
        {
            m_list = Gl.glGenLists(1);
            Gl.glNewList(m_list, Gl.GL_COMPILE);

            #region Draw Axes

            Gl.glPushMatrix();

            Gl.glColor3f(1.0f, 0.0f, 0.0f);

            // draw a line along the y-axis //
            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(0.0f, -this.AxisSize, 0.0f);
            Gl.glVertex3f(0.0f, this.AxisSize, 0.0f);
            Gl.glEnd();

            // draw a line along the x-axis //
            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-this.AxisSize, 0.0f, 0.0f);
            Gl.glVertex3f(this.AxisSize, 0.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-1.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-1.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-2.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-2.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-3.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-3.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-4.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-4.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-5.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-5.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-6.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-6.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-7.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-7.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-8.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-8.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-9.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-9.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-10.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-10.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-11.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-11.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-12.0f, 0.2f, 0.0f);
            Gl.glVertex3f(-12.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(1.0f, 0.2f, 0.0f);
            Gl.glVertex3f(1.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(2.0f, 0.2f, 0.0f);
            Gl.glVertex3f(2.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(3.0f, 0.2f, 0.0f);
            Gl.glVertex3f(3.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(4.0f, 0.2f, 0.0f);
            Gl.glVertex3f(4.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(5.0f, 0.2f, 0.0f);
            Gl.glVertex3f(5.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(6.0f, 0.2f, 0.0f);
            Gl.glVertex3f(6.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(7.0f, 0.2f, 0.0f);
            Gl.glVertex3f(7.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(8.0f, 0.2f, 0.0f);
            Gl.glVertex3f(8.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(9.0f, 0.2f, 0.0f);
            Gl.glVertex3f(9.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(10.0f, 0.2f, 0.0f);
            Gl.glVertex3f(10.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(11.0f, 0.2f, 0.0f);
            Gl.glVertex3f(11.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(12.0f, 0.2f, 0.0f);
            Gl.glVertex3f(12.0f, -0.2f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 1.0f, 0.0f);
            Gl.glVertex3f(0.2f, 1.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 2.0f, 0.0f);
            Gl.glVertex3f(0.2f, 2.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 3.0f, 0.0f);
            Gl.glVertex3f(0.2f, 3.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 4.0f, 0.0f);
            Gl.glVertex3f(0.2f, 4.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 5.0f, 0.0f);
            Gl.glVertex3f(0.2f, 5.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 6.0f, 0.0f);
            Gl.glVertex3f(0.2f, 6.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 6.0f, 0.0f);
            Gl.glVertex3f(0.2f, 6.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 7.0f, 0.0f);
            Gl.glVertex3f(0.2f, 7.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 8.0f, 0.0f);
            Gl.glVertex3f(0.2f, 8.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 9.0f, 0.0f);
            Gl.glVertex3f(0.2f, 9.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 10.0f, 0.0f);
            Gl.glVertex3f(0.2f, 10.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 11.0f, 0.0f);
            Gl.glVertex3f(0.2f, 11.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, 12.0f, 0.0f);
            Gl.glVertex3f(0.2f, 12.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -1.0f, 0.0f);
            Gl.glVertex3f(0.2f, -1.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -2.0f, 0.0f);
            Gl.glVertex3f(0.2f, -2.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -3.0f, 0.0f);
            Gl.glVertex3f(0.2f, -3.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -4.0f, 0.0f);
            Gl.glVertex3f(0.2f, -4.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -5.0f, 0.0f);
            Gl.glVertex3f(0.2f, -5.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -6.0f, 0.0f);
            Gl.glVertex3f(0.2f, -6.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -7.0f, 0.0f);
            Gl.glVertex3f(0.2f, -7.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -8.0f, 0.0f);
            Gl.glVertex3f(0.2f, -8.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -9.0f, 0.0f);
            Gl.glVertex3f(0.2f, -9.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -10.0f, 0.0f);
            Gl.glVertex3f(0.2f, -10.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -11.0f, 0.0f);
            Gl.glVertex3f(0.2f, -11.0f, 0.0f);
            Gl.glEnd();

            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-0.2f, -12.0f, 0.0f);
            Gl.glVertex3f(0.2f, -12.0f, 0.0f);
            Gl.glEnd();

            Gl.glPopMatrix();

            #endregion Draw Axes

            Gl.glEndList();
        }

        public void Draw(bool picking)
        {
            #region Movement and Draw Axes

            Gl.glPushMatrix();

            Gl.glTranslatef(0f, 0f, axesZTranslated);

            Gl.glRotatef(this.axesXLocation, 1f, 0f, 0f);
            Gl.glRotatef(this.axesYLocation, 0f, 1f, 0f);
            Gl.glRotatef(this.axesZLocation, 0f, 0f, 1f);

            Gl.glCallList(m_list);

            Gl.glPopMatrix();

            #endregion Movement and Draw Axes
        }

        public void Pan(Ponto2D ponto)
        {
        }

        public int ID
        {
            get { return this.m_id; }
            set { this.m_id = value; }
        }

        public Dictionary<int, GroupingElements> InternalDraws
        {
            get
            {
                return null;
            }
            set { }
        }

        #endregion IDrawable

        public string WritePoints()
        {
            return null;
        }
    }
}