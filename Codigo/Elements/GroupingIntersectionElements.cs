﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control.Elements
{
    public class GroupingIntersectionElements
    {
        #region Fields

        private GroupingElements elements;

        #endregion Fields

        public GroupingElements Elements
        {
            get { return this.elements; }
            set { this.elements = value; }
        }

        #region Constructor

        public GroupingIntersectionElements()
        {
            elements = new GroupingElements();
        }

        #endregion Constructor
    }
}