﻿using System.Collections.Generic;
using openGl_Control.Elements;

namespace openGl_Control
{
    public interface IDrawable
    {
        Dictionary<int, GroupingElements> InternalDraws
        {
            get;
            set;
        }

        int ID
        {
            get;
            set;
        }

        void Draw();

        void Draw(bool picking);

        void InitializePrimitives();

        void Rotate(Ponto2D ponto);

        void Pan(Ponto2D ponto);

        void Zoom(int delta);

        string WritePoints();
    }
}