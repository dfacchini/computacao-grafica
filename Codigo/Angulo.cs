﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using openGl_Control.Elements;

namespace openGl_Control
{
    public enum TipoAngulo : int
    {
        Radiano = 1,
        Degradiano = 2,
        Cosseno = 4,
        Seno = 8,
        Tangente = 16
    }

    public class Angulo
    {
        private double m_degradiano;

        public Angulo(TipoAngulo tipo, double valor)
        {
            AlteraValor(tipo, valor);
        }

        public Angulo()
            : this(TipoAngulo.Degradiano, 0)
        {
        }

        public static implicit operator double(Angulo angulo)
        {
            return angulo.Degradianos;
        }

        public void AlteraValor(TipoAngulo tipo, double valor)
        {
            switch (tipo)
            {
                case TipoAngulo.Cosseno:
                    Cosseno = valor;
                    break;
                case TipoAngulo.Degradiano:
                    Degradianos = valor;
                    break;
                case TipoAngulo.Radiano:
                    Radianos = valor;
                    break;
                case TipoAngulo.Seno:
                    Seno = valor;
                    break;
                case TipoAngulo.Tangente:
                    Tangente = valor;
                    break;
            }

        }

        public double Cosseno
        {
            get
            {
                return Math.Cos(Radianos);
            }
            set
            {
                if (value < -1)
                    value = -1;
                else if (value > 1)
                    value = 1;
                Radianos = Math.Acos(value);
            }
        }

        public double Seno
        {
            get
            {
                return Math.Sin(Radianos);
            }
            set
            {
                if (value < -1)
                    value = -1;
                else if (value > 1)
                    value = 1;
                Radianos = Math.Asin(value);
            }
        }

        public double Tangente
        {
            get
            {
                return Math.Tan(Radianos);
            }
            set
            {
                Radianos = Math.Atan(value);
            }
        }

        public double Radianos
        {
            get
            {
                return Rad(this.Degradianos);
            }
            set
            {
                double pi2 = 2 * Math.PI;
                double radianos = value % pi2;

                if (radianos < 0)
                    radianos += pi2;

                Degradianos = Deg(radianos);
            }
        }

        public static double Deg(double rad)
        {
            return rad * 180d / Math.PI;
        }

        public static double Rad(double deg)
        {
            return deg * Math.PI / 180d;
        }

        public static bool ValorEqual(float valor1, float valor2)
        {
            return ValorEqual(valor1, valor2, 0.001);
        }

        public static bool ValorEqual(double valor1, double valor2)
        {
            return ValorEqual(valor1, valor2, 0.001);
        }

        public static bool ValorEqual(float valor1, float valor2, float tolerancia)
        {
            return (
                    ((valor1 >= (valor2 - tolerancia)) && (valor1 <= (valor2 + tolerancia)))
                    ||
                    ((valor2 >= (valor1 - tolerancia)) && (valor2 <= (valor1 + tolerancia)))
                    );
        }

        public static bool ValorEqual(double valor1, double valor2, double tolerancia)
        {
            return (
                    ((valor1 >= (valor2 - tolerancia)) && (valor1 <= (valor2 + tolerancia)))
                    ||
                    ((valor2 >= (valor1 - tolerancia)) && (valor2 <= (valor1 + tolerancia)))
                    );
        }

        public double Degradianos
        {
            get
            {
                return m_degradiano;
            }
            set
            {
                m_degradiano = value % 360;

                if (m_degradiano < 0)
                    m_degradiano += 360d;
            }
        }

        public static Angulo EntreDoisPontos(double x1, double y1, double x2, double y2)
        {
            return EntreDoisPontos(x1, y1, x2, y2, false);
        }

        public static Angulo EntreDoisPontos(double x1, double y1, double x2, double y2, bool considerarProximosZero)
        {
            Angulo anguloRetorno = new Angulo();
            double anguloRadiano;

            double diffX = x2 - x1;
            double diffY = y2 - y1;
            if (ValorEqual(diffX, 0, 0.01)) // Diferenca em X muito proxima de 0 angulo igual a 90 ou 270
            {
                anguloRetorno.AlteraValor(
                    TipoAngulo.Degradiano,
                    ((y2 > y1) ? 90 : (y1 > y2) ? 270 : 0)
                    );
            }
            else
            {
                anguloRadiano = Math.Atan(diffY / diffX);
                if (diffX < 0)
                    anguloRadiano += Math.PI;

                if (!considerarProximosZero && ValorEqual(anguloRadiano, 0))
                    anguloRadiano = 0;

                anguloRetorno.AlteraValor(TipoAngulo.Radiano, anguloRadiano);
            }

            return anguloRetorno;
        }

        public static Angulo EntreDoisPontos(Line linha)
        {
            return Angulo.EntreDoisPontos(linha.PontoInicial, linha.PontoFinal);
        }

        public static Angulo EntreDoisPontos(Ponto2D ponto1, Ponto2D ponto2)
        {
            return Angulo.EntreDoisPontos(ponto1.X, ponto1.Y, ponto2.X, ponto2.Y);
        }

        public static Angulo operator -(Angulo ang1, Angulo ang2)
        {
            return new Angulo(TipoAngulo.Degradiano, (ang1.Degradianos - ang2.Degradianos));
        }

        public static Angulo operator +(Angulo ang1, Angulo ang2)
        {
            return new Angulo(TipoAngulo.Degradiano, (ang1.Degradianos + ang2.Degradianos));
        }

        public static Angulo operator +(Angulo ang1, double incrementoDegradiano)
        {
            return new Angulo(TipoAngulo.Degradiano, (ang1.Degradianos + incrementoDegradiano));
        }

        public override string ToString()
        {
            return String.Concat(Degradianos.ToString(), "º");
        }

        public object Clone()
        {
            return new Angulo(TipoAngulo.Radiano, Radianos);
        }

        public static bool Equivalente(Angulo ang1, Angulo ang2)
        {
            if (ValorEqual(ang1.Degradianos, 0d) || ValorEqual(ang1.Degradianos, 360d))
                return ValorEqual(ang2.Degradianos, 0d) || ValorEqual(ang2.Degradianos, 360d);

            return ValorEqual(ang1.Degradianos, ang2.Degradianos);
        }

        public static bool EstaDentro(Angulo angInicial, Angulo angFinal, bool antiHorario, Angulo angTeste)
        {
            return EstaDentro(angInicial, angFinal, antiHorario, angTeste, null);
        }

        public static bool EstaDentro(Angulo angInicial, Angulo angFinal, bool antiHorario, Angulo angTeste, double tolerancia)
        {
            return EstaDentro(angInicial, angFinal, antiHorario, angTeste, (double?)tolerancia);
        }

        private static bool EstaDentro(Angulo angInicial, Angulo angFinal, bool antiHorario, Angulo angTeste, double? tolerancia)
        {
            double inicial = angInicial.Degradianos;
            double final = angFinal.Degradianos;
            double teste = angTeste.Degradianos;

            if (tolerancia == null)
            {
                if (ValorEqual(inicial, teste))
                    return true;
                if (ValorEqual(final, teste))
                    return true;
            }
            else
            {
                if (ValorEqual(inicial, teste, tolerancia.Value))
                    return true;
                if (ValorEqual(final, teste, tolerancia.Value))
                    return true;
            }

            if (antiHorario)
            {
                if (final > inicial)
                    inicial += 360d;
                if (teste < final)
                    teste += 360d;

                return teste < inicial;
            }
            else
            {
                if (inicial > final)
                    final += 360d;
                if (teste < inicial)
                    teste += 360d;

                return teste < final;
            }
        }        
    }
}
